import React from "react";
import { ActivityIndicator, StyleSheet, Text, View } from "react-native";
import { AppNavigator } from "./routes/AppNavigator";
import { useFonts } from "expo-font";
export default function App() {

  let [fontsLoaded] = useFonts({
    MB: require("./assets/fonts/Montserrat-Bold.ttf"),
    MM: require("./assets/fonts/Montserrat-Medium.ttf"),
    MR: require("./assets/fonts/Montserrat-Regular.ttf"),
    MS: require("./assets/fonts/Montserrat-SemiBold.ttf"),
  });

  if (!fontsLoaded) {
    return (
      <ActivityIndicator
        size="large"
        color="#222"
        style={{ alignSelf: "center" }}
      />
    );
  }


  return <AppNavigator />;
}
