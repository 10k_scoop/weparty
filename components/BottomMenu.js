import React from "react";
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { FontAwesome, MaterialCommunityIcons } from "@expo/vector-icons";

export default function BottomMenu(props) {
  return (
    <View style={styles.container}>
      <View style={styles.IconRow}>
        <TouchableOpacity
          style={styles.Icon}
          onPress={() => props.navigation.navigate("Home")}
        >
          <MaterialCommunityIcons
            name="home-outline"
            size={rf(22)}
            color="#FF5623"
          />
          <Text style={styles.Font}>WeParty</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.IconRow}>
        <TouchableOpacity
          style={styles.Icon}
          onPress={() => props.navigation.navigate("Map")}
        >
          <MaterialCommunityIcons
            name="map-marker-multiple"
            size={rf(22)}
            color="grey"
          />
          <Text style={styles.Font2}>Maps</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.IconRow}>
        <TouchableOpacity
          style={styles.Icon}
          onPress={() => props.navigation.navigate("Profile")}
        >
          <FontAwesome name="user-circle" size={rf(22)}
            color="grey" />
          <Text style={styles.Font2}>Profile</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("100%"),
    height: hp("8%"),
    borderWidth: 1,
    borderColor: "#e5e5e5",
    position: "absolute",
    bottom: 0,
    backgroundColor: "#fff",
    flexDirection: "row",
    justifyContent: "space-evenly",
  },
  IconRow: {
    width: "33%",
    height: "100%",
    alignItems: "center",
    justifyContent: "space-evenly",
    paddingVertical: 5,
  },
  Font: {
    fontSize: rf(12),
    fontFamily: 'MM',
    color: "#FF5623",
  },
  Font2: {
    fontSize: rf(12),
    fontFamily: 'MM',
    color: "grey",
    letterSpacing: 0.6
  },
  Icon: {
    alignItems: "center",
  },
});
