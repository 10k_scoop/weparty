import React from "react";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { Ionicons } from "@expo/vector-icons";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
export default function Header(props) {
  return (
    <View style={styles.container}>
      {props.back ? null : (
        <TouchableOpacity onPress={props.navigation} style={{flexDirection:'row',alignItems:'center'}}>
          <Ionicons name="chevron-back" size={rf(20)} color="#FF5623" />
          <Text style={styles.BackText}>{props.BackTxt}</Text>
        </TouchableOpacity>
      )}

      <View style={styles.TitleBody}>
        <Text style={styles.TitleText}>{props.Title}</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    height: hp("14%"),
    paddingHorizontal: wp("4%"),
    flexDirection: "row",
    alignItems: "center",
  },

  BackText: {
    fontSize: rf(14),
    fontWeight: "700",
    color: "#FF5623",
    marginLeft: 5,
  },
  TitleBody: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  TitleText: {
    fontSize: rf(16),
    fontFamily:'MB',
    letterSpacing:0.8,
    color: "#222",
  },
});
