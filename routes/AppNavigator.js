import * as React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import Login from "../screens/Login/Login";
import Home from "../screens/Home/Home";
import Map from "../screens/Maps/Map";
import MapList from "../screens/Map-List/MapList";
import PointerMap from "../screens/Pointer-Map/PointerMap";
import Event from "../screens/Event/Event";
import Favourites from "../screens/favourites/Favourites";
import ThirdCalender from "../screens/Third-Calender/ThirdCalender";
import Info from "../screens/Info/Info";
import FAQ from "../screens/FAQ/FAQ";
import Support from "../screens/Support/Support";
import Security from "../screens/Security/Security";
import Reason from "../screens/Reason/Reason";
import Profile from "../screens//Profile/Profile";
import EditProfile from "../screens/EditProfile/EditProfile";
import Calender from "../screens/Calender/Calender";
import SecondCalender from "../screens/SecondCalender/SecondCalender";
import Filter from "../screens/Filter/Filter";
import FilterMusic from "../screens/FilterMusic/FilterMusic";
import FilterClub from "../screens/FilterClub/FilterClub";
import onBoarding from "../screens/onBoardingScreen/onBoarding";
import Signup from "../screens/Signup/Signup";
import Bars from "../screens/Bars/Bars";
import DJPage from "../screens/DJ/DJPage";
import Barrin from "../screens/Barrin/Barrin";

const { Navigator, Screen } = createStackNavigator();

function AppNavigation() {
  return (
    <Navigator screenOptions={{ headerShown: false }}>
      <Screen name="onBoarding" component={onBoarding} />
      <Screen name="Login" component={Login} />
      <Screen name="Barrin" component={Barrin} />
      <Screen name="Signup" component={Signup} />
      <Screen name="Home" component={Home} />
      <Screen name="Event" component={Event} />
      <Screen name="Map" component={Map} />
      <Screen name="MapList" component={MapList} />
      <Screen name="PointerMap" component={PointerMap} />
      <Screen name="Profile" component={Profile} />
      <Screen name="EditProfile" component={EditProfile} />
      <Screen name="Favourites" component={Favourites} />
      <Screen name="ThirdCalender" component={ThirdCalender} />
      <Screen name="Bars" component={Bars} />
      <Screen name="DJPage" component={DJPage} />
      <Screen name="Info" component={Info} />
      <Screen name="FAQ" component={FAQ} />
      <Screen name="Support" component={Support} />
      <Screen name="Security" component={Security} />
      <Screen name="Reason" component={Reason} />
      <Screen name="Calender" component={Calender} />
      <Screen name="SecondCalender" component={SecondCalender} />
      <Screen name="Filter" component={Filter} />
      <Screen name="FilterMusic" component={FilterMusic} />
      <Screen name="FilterClub" component={FilterClub} />
    </Navigator>
  );
}
export const AppNavigator = () => (
  <NavigationContainer>
    <AppNavigation />
  </NavigationContainer>
);
