import React, { useState } from "react";
import { StyleSheet, Text, View, Image, ScrollView } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import Header from "./components/Header";
import SearchBar from "./components/SearchBar";
import ButtonsBar from "./components/ButtonsBar";
import EventCard from "./components/EventCard";
import PartyCards from "./components/PartyCards";
import BottomMenu from "../../components/BottomMenu";

export default function Home({ navigation }) {
  const [optionSelected, setOptionSelected] = useState("Clubbing");

  return (
    <View style={styles.container}>
      <Header
        OnAdd={() => navigation.navigate("Calender")}
        onClick={() => navigation.navigate("Info")}
        Click={() => navigation.navigate("Filter")}
      />
      <SearchBar />
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.Wrapper}>
          <ButtonsBar
            onClubbingPress={() => setOptionSelected("Clubbing")}
            onBarringress={() => navigation.navigate("Barrin")}
            onFavouritePress={() => setOptionSelected("Favourites")}
            option={optionSelected}
          />
          {optionSelected == "Clubbing" ? (
            <>
              <View style={styles.Discription}>
                <Text style={styles.Font}>Hottest this week 🔥</Text>
              </View>
              <EventCard />
              <PartyCards onPress={() => navigation.navigate("Event",{type:'event'})} />
              <PartyCards onPress={() => navigation.navigate("Event",{type:'event'})} />
              <PartyCards onPress={() => navigation.navigate("Event",{type:'event'})} />
            </>
          ) : (
            <>
              <PartyCards
                favourite
                onPress={() => navigation.navigate("Event")}
              />
              <PartyCards
                favourite
                onPress={() => navigation.navigate("Event")}
              />
              <PartyCards
                favourite
                onPress={() => navigation.navigate("Event")}
              />
            </>
          )}
        </View>
      </ScrollView>
      <BottomMenu navigation={navigation} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  Wrapper: {
    width: wp("100%"),
    alignItems: "center",
    marginBottom: "10%",
  },
  Discription: {
    width: wp("95%"),
    height: hp("6%"),
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: wp("2%"),
  },
  Font: {
    fontSize: rf(13),
    fontFamily: "MB",
    letterSpacing: 0.5,
  },
});
