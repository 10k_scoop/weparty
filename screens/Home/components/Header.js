import React from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { AntDesign, Feather } from "@expo/vector-icons";

export default function Header(props) {
  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={props.onClick}>
        <AntDesign name="infocirlce" size={rf(18)} color="#FD581F" />
      </TouchableOpacity>
      <View>
        <Text style={styles.Font}>WeParty</Text>
      </View>
      <View style={styles.IconsView}>
        <TouchableOpacity onPress={props.OnAdd}>
          <Feather name="calendar" size={rf(23)} color="#FD581F" />
        </TouchableOpacity>
        <TouchableOpacity style={{ marginLeft: 15 }} onPress={props.Click}>
          <AntDesign name="filter" size={rf(23)} color="#FD581F" />
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("100%"),
    height: hp("10%"),
    backgroundColor: "#fff",
    alignItems: "flex-end",
    paddingVertical: 10,
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: wp("5%"),
  },
  IconsView: {
    flexDirection: "row",
  },
  Font: {
    fontSize: rf(16),
    fontWeight: "700",
    left: 15,
    fontFamily:'MB',
    letterSpacing:1
  },
});
