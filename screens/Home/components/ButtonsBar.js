import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { Feather } from "@expo/vector-icons";

export default function ButtonsBar(props) {
  return (
    <View style={styles.container}>
      <View style={styles.Layer}></View>
      <TouchableOpacity
        onPress={props.onClubbingPress}
        style={[
          styles.AllButton,
          { backgroundColor: props.option === "Clubbing" ? "#fff" : "transparent" },
        ]}
      >
        <Text
          style={[
            styles.ButtonFont,
            { color: props.option === "Clubbing" ? "#FD581F" : "#222" },
          ]}
        >
          Clubbing
        </Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={props.onBarringress}
        style={[
          styles.AllButton,
          { backgroundColor: props.option === "Barring" ? "#fff" : "transparent" },
        ]}
      >
        <Text
          style={[
            styles.ButtonFont,
            { color: props.option === "Barring" ? "#FD581F" : "#222" },
          ]}
        >
          Barring
        </Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={props.onFavouritePress}
        style={[
          styles.FavouritesButton,
          {
            backgroundColor:
              props.option === "Favourites" ? "#fff" : "transparent",
          },
        ]}
      >
        <Text
          style={[
            styles.ButtonFont,
            { color: props.option === "Favourites" ? "#FD581F" : "#222" },
          ]}
        >
          Favourites
        </Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("95%"),
    height: hp("5%"),
    borderRadius: 10,
    flexDirection: "row",
    overflow: "hidden",
    alignItems: "center",
    marginTop: 10,
    justifyContent: "center",
   
  },
  Layer: {
    width: "100%",
    height: "100%",
    backgroundColor: "#E5E5E5",
    opacity: 0.4,
    position: "absolute",
  },
  AllButton: {
    flex:1,
    height: "90%",
    backgroundColor: "#fff",
    borderRadius: 10,
    alignItems: "center",
    justifyContent: "center",
    marginHorizontal:3
  },
  FavouritesButton: {
    flex:1,
    height: "90%",
    backgroundColor: "red",
    borderRadius: 10,
    alignItems: "center",
    justifyContent: "center",
    marginHorizontal:3
  },
  ButtonFont: {
    fontSize: rf(12),
    fontFamily:'MM',
    letterSpacing:1
  },
});
