import React, { useState } from "react";
import { StyleSheet, Text, View, Image } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { AntDesign, Feather } from "@expo/vector-icons";

export default function EventCard(props) {
  return (
    <View style={styles.container}>
      <View style={styles.Wraaper}>
        <View style={styles.FirstRow}>
          <Text style={styles.Font}>WeParty</Text>
          <View style={styles.Img}>
            <Image
              source={require("../../../assets/EventPic.png")}
              resizeMode="cover"
              style={{ width: "100%", height: "100%" }}
            />
          </View>
        </View>
        <View style={styles.SecondRow}>
          <View>
            <Text style={styles.Font}>Lorem Ipsum</Text>
            <Text style={styles.Font2}>Lorem Ipsum 1017 Amsterdam </Text>
          </View>
          <View style={styles.Img}>
            <Image
              source={require("../../../assets/EventPic.png")}
              resizeMode="cover"
              style={{ width: "100%", height: "100%" }}
            />
          </View>
        </View>
        <View style={styles.ThirdRow}>
          <View>
            <Text style={styles.Font}>Lorem Ipsum</Text>
            <Text style={styles.Font2}>Lorem Ipsum 1017 Amsterdam </Text>
          </View>
          <View style={styles.Img}>
            <Image
              source={require("../../../assets/EventPic.png")}
              resizeMode="cover"
              style={{ width: "100%", height: "100%" }}
            />
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("95%"),
    height: hp("20%"),
    borderColor: "#FD581F",
    borderRadius: 10,
    alignItems: "center",
    justifyContent: "center",
    marginBottom: "5%",
    borderWidth: 2,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  Wraaper: {
    width: "100%",
    height: "100%",
    backgroundColor: "#fff",
    borderRadius: 10,
    alignItems: "flex-end",
    justifyContent: "space-around",
  },
  FirstRow: {
    width: "95%",
    height: "32%",
    borderBottomWidth: 2,
    borderColor: "#e5e5e5",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  Img: {
    width: "32%",
    height: "80%",
    borderRadius: 10,
    marginRight: 7,
    overflow: "hidden",
  },
  Font: {
    fontSize: rf(13),
    color: "#FF5623",
    fontFamily: "MB",
    letterSpacing: 0.8,
  },
  SecondRow: {
    width: "95%",
    height: "32%",
    borderBottomWidth: 1,
    borderColor: "#e5e5e5",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  ThirdRow: {
    width: "95%",
    height: "32%",
    borderColor: "#e5e5e5",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  Font2: {
    fontSize: rf(12),
    fontWeight: "400",
    color: "grey",
  },
});
