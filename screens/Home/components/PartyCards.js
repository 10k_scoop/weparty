import React from "react";
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  TouchableOpacity,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { AntDesign, MaterialCommunityIcons } from "@expo/vector-icons";

export default function PartyCards(props) {
  return (
    <View style={styles.container}>
      <Text style={styles.DateFont}>Sunday 14 November</Text>
      <TouchableOpacity style={styles.Card} onPress={() => props.onPress()}>
        <View style={styles.FirstRow}>
          <ImageBackground
            source={require("../../../assets/PartyPic.jpeg")}
            style={{ width: "100%", height: "100%" }}
            resizeMode="cover"
          >
            <View style={styles.Inner}>
              <TouchableOpacity
                style={styles.Ticket}
                onPress={() => props.onPress()}
              >
                <Text style={styles.TicketFont}>Get Tickets</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.heartIcon}>
                {props.favourite ? (
                  <AntDesign name="heart" size={24} color="red" />
                ) : (
                  <AntDesign name="hearto" size={rf(22)} color="#fff" />
                )}
              </TouchableOpacity>
            </View>
          </ImageBackground>
        </View>
        <View style={styles.SecondRow}>
          <View>
            <Text style={styles.Font1}>Lorem Ipsum</Text>
            <Text style={styles.Font2}>Lorem Ipsum is simply dummy text</Text>
          </View>
          <View style={styles.Inner2}>
            <MaterialCommunityIcons
              name="account"
              size={rf(22)}
              color="#FF5623"
            />
            <Text style={styles.Font1}>4</Text>
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("95%"),
    height: hp("27%"),
    justifyContent: "space-around",
    marginBottom: hp("1%"),
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  DateFont: {
    fontSize: rf(14),
    fontFamily: "MM",
    marginLeft: 10,
  },
  heartIcon: {
    position: "absolute",
    right: 5,
    top: 10,
  },
  Card: {
    width: "100%",
    height: "85%",
    borderRadius: 10,
    overflow: "hidden",
    backgroundColor: "#fff",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  FirstRow: {
    width: "100%",
    height: "70%",
    backgroundColor: "grey",
  },
  Inner: {
    width: "100%",
    height: "40%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingHorizontal: "2%",
  },
  Ticket: {
    width: wp("35%"),
    height: hp("4%"),
    backgroundColor: "#FD581F",
    justifyContent: "center",
    alignItems: "center",
    borderTopLeftRadius: 1000,
    borderBottomRightRadius: 1000,
    position: "absolute",
    top: -1,
    left: -20,
  },
  TicketFont: {
    fontSize: rf(13),
    fontFamily: "MB",
    color: "#fff",
  },
  SecondRow: {
    width: "100%",
    height: "30%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingHorizontal: "4%",
  },
  Font1: {
    fontSize: rf(14),
    fontFamily: "MB",
    color: "#FF5623",
    letterSpacing: 0.4,
  },
  Font2: {
    fontSize: rf(12),
    fontFamily: "MR",
    color: "grey",
  },
  Inner2: {
    width: "10%",
    height: "100%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
});
