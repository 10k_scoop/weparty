import React from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { AntDesign, Feather } from "@expo/vector-icons";

export default function DetailCard(props) {
  return (
    <View style={styles.detailView}>
      <Text style={styles.Font1}>{props.txt1}</Text>
      <Text style={styles.Font2}>{props.txt2}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  detailView: {
    width: wp("90%"),
    height: hp("6%"),
    justifyContent: "center",
  },
  Font1: {
    fontSize: rf(14),
    fontFamily:'MB',
    letterSpacing:0.8,
    color: "#FF5623",
  },
  Font2: {
    fontSize: rf(11),
    fontFamily:'MM',
    letterSpacing:0.8,
    color: "#525151",
    top: 1,
  },
});
