import React from "react";
import { StyleSheet, Text, View, TouchableOpacity, Image } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { AntDesign, Foundation } from "@expo/vector-icons";

export default function ShareButton(props) {
  return (
    <TouchableOpacity style={styles.container}>
      <AntDesign name="sharealt" size={18} color="#FD581F" />
      <Text style={styles.Font}>{props.type=="event"?"Share Event":"Share Bar"}</Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("90%"),
    height: hp("5%"),
    flexDirection: "row",
    borderWidth: 1.5,
    borderColor: "#FD581F",
    borderRadius: 10,
    alignItems: "center",
    justifyContent: "center",
    marginBottom: "5%",
  },
  Font: {
    fontSize: rf(14),
    fontFamily:'MB',
    color: "#FD581F",
    marginLeft: 10,
  },
});
