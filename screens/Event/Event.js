import React from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ScrollView,
  ImageBackground,
  Button,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import Header from "./components/Header";
import FavouritesCard from "./components/FavouritesCard";
import TicketButton from "./components/TicketButton";
import ShareButton from "./components/ShareButton";
import DetailCard from "./components/DetailCard";
import BottomMenu from "../../components/BottomMenu";
import { Video, AVPlaybackStatus } from "expo-av";
import MapView from "react-native-maps";

export default function Event({ navigation, route }) {
  const video = React.useRef(null);
  const [status, setStatus] = React.useState({});
  const type = route.params.type;
  return (
    <View style={styles.container}>
      <Header
        title={type == "bars" ? "Bar1" : "Lorem Ipsum"}
        tagline={type == "bars" ? "9:00 am - 5:00 am" : "Wednesday 14 October"}
        onBackClick={() => navigation.goBack()}
      />
      <ScrollView>
        <View style={styles.wrapper}>
          <View style={styles.Img}>
            <ImageBackground
              source={require("../../assets/PartyPic.jpeg")}
              style={{ width: "100%", height: "100%" }}
              resizeMode="cover"
            ></ImageBackground>
          </View>
          <FavouritesCard type={type} />
          <TicketButton type={type} />
          <ShareButton type={type} />
          <DetailCard
            txt1={type == "event" ? "Lorem Ipsum" : "Bar 1"}
            txt2={
              type == "event"
                ? "Wednesday 14 October From 09:00 - 10:00"
                : "Opening hours \n9:00 am - 5:00 am"
            }
          />
          <DetailCard txt1="Location" txt2="42, 1027 BM Amsterdam" />
          <DetailCard
            txt1="Ticket Price"
            txt2={type == "event" ? "$ 9,00" : "Free"}
          />
          <DetailCard txt1="Age" txt2="18-30" />
          <DetailCard
            txt1="Genre"
            txt2="90’s//00 guilty pleasure ,R&B pleasure"
          />
          <View style={styles.Discription}>
            <Text style={styles.Font1}>Description</Text>
            <Text style={styles.Font2}>
              Lorem Ipsum is simply dummy text of the{"\n"}printing and
              typesetting industry. Lorem{"\n"}Ipsum has been the industry's
              standard{"\n"}dummy text ever since the 1500s
            </Text>
          </View>
          <TouchableOpacity
            style={styles.DjCont}
            onPress={() => navigation.navigate("DJPage")}
          >
            <Text style={styles.DjText}>DJ</Text>
          </TouchableOpacity>
          <View style={styles.PhotosView}>
            <ScrollView
              horizontal={true}
              showsHorizontalScrollIndicator={false}
              pagingEnabled
            >
              <ImageBackground
                source={require("../../assets/Dancing.png")}
                style={{
                  width: wp("100%"),
                  height: "100%",
                  justifyContent: "flex-end",
                }}
                resizeMode="stretch"
              >
                <View style={styles.DotsWrapper}>
                  <View style={styles.Dot1}></View>
                  <View style={styles.Dot2}></View>
                  <View style={styles.Dot2}></View>
                </View>
              </ImageBackground>
              <ImageBackground
                source={require("../../assets/Dancing.png")}
                style={{
                  width: wp("100%"),
                  height: "100%",
                  justifyContent: "flex-end",
                }}
              >
                <View style={styles.DotsWrapper}>
                  <View style={styles.Dot2}></View>
                  <View style={styles.Dot1}></View>
                  <View style={styles.Dot2}></View>
                </View>
              </ImageBackground>
              <ImageBackground
                source={require("../../assets/Dancing.png")}
                style={{
                  width: wp("100%"),
                  height: "100%",
                  justifyContent: "flex-end",
                }}
              >
                <View style={styles.DotsWrapper}>
                  <View style={styles.Dot2}></View>
                  <View style={styles.Dot2}></View>
                  <View style={styles.Dot1}></View>
                </View>
              </ImageBackground>
            </ScrollView>
          </View>
          <View style={styles.videoContainer}>
            <Video
              ref={video}
              style={styles.video}
              source={{
                uri: "http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4",
              }}
              useNativeControls
              resizeMode="cover"
              isLooping
              onPlaybackStatusUpdate={(status) => setStatus(() => status)}
            />
          </View>
          <View style={styles.mapContainer}>
            <MapView
              style={styles.map}
              region={{
                latitude: 52.377956,
                longitude: 4.88969,
                latitudeDelta: 0.015,
                longitudeDelta: 0.0121,
              }}
            />
          </View>
        </View>
      </ScrollView>
      <BottomMenu navigation={navigation} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  wrapper: {
    width: wp("100%"),
    alignItems: "center",
    marginBottom: "17%",
  },
  Img: {
    width: wp("100%"),
    height: hp("20%"),
    backgroundColor: "red",
  },
  Discription: {
    width: wp("90%"),
    paddingVertical: 10,
  },
  Font1: {
    fontSize: rf(14),
    fontFamily: "MB",
    letterSpacing: 0.8,
    color: "#FF5623",
  },
  Font2: {
    fontSize: rf(12),
    fontFamily: "MM",
    letterSpacing: 1,
    color: "#525151",
    marginTop: 5,
  },
  title: {
    width: wp("100%"),
    height: hp("5%"),
    justifyContent: "center",
    backgroundColor: "#E5E5E5",
    paddingHorizontal: "5%",
  },
  titleFont: {
    fontSize: rf(13),
    fontFamily: "MB",
    letterSpacing: 0.8,
  },
  PhotosView: {
    height: hp("24%"),
    flexDirection: "row",
    marginTop: 5,
  },
  DotsWrapper: {
    width: "100%",
    height: "25%",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  Dot1: {
    width: hp("1%"),
    height: hp("1%"),
    borderRadius: 100,
    borderWidth: 0,
    backgroundColor: "#FD581F",
    margin: 2,
  },
  Dot2: {
    width: hp("1%"),
    height: hp("1%"),
    borderRadius: 100,
    borderWidth: 2,
    borderColor: "#FD581F",
    backgroundColor: "#fff",
    margin: 2,
  },
  videoContainer: {
    width: "100%",
    height: hp("18%"),
    marginVertical: 10,
  },
  video: {
    width: "100%",
    height: hp("18%"),
  },
  map: {
    width: wp("100%"),
    height: hp("27%"),
  },
  DjCont: {
    width: wp("100%"),
    height: hp("7%"),
    paddingVertical: 10,
    paddingHorizontal: wp("5%"),
    backgroundColor: "#fff",
    borderBottomWidth: 1,
    borderTopWidth: 1,
    borderColor: "#e5e5e5",
    justifyContent: "center",
  },
  DjText: {
    fontFamily: "MB",
    fontSize: rf(16),
    color: "#FF5623",
  },
});
