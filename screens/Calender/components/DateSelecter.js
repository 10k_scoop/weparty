import React from "react";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { StyleSheet, Text, View, TextInput } from "react-native";
import { AntDesign } from "@expo/vector-icons";
export default function DateSelecter({ navigation }) {
  return (
    <View style={styles.container}>
      <View style={styles.StartingDateBody}>
        <Text style={styles.StartingDateText}>Select</Text>
        <Text style={styles.StartingDateText}>Starting</Text>
        <Text style={styles.StartingDateText}>date</Text>
        <TextInput placeholder="e.g 19/10/2021" style={styles.InputDateText} />
      </View>
      <View style={styles.CalenderIconBody}>
        <View style={styles.CalenderIcon}>
          <AntDesign name="calendar" size={rf(28)} color="#FF5623" />
        </View>
      </View>
      <View style={styles.StartingDateBody}>
        <Text style={styles.StartingDateText}>Select</Text>
        <Text style={styles.StartingDateText}>Ending</Text>
        <Text style={styles.StartingDateText}>date</Text>
        <TextInput placeholder="e.g 19/10/2021" style={styles.InputDateText} />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    borderTopWidth: 2,
    borderTopColor: "#e5e5e5",
    borderColor: "#B6ADAD",
    alignItems: "center",
    flexDirection: "row",
    paddingHorizontal: "4%",
    justifyContent: "space-between",
  },
  StartingDateBody: {
    height: "100%",
    width: "25%",
    alignItems: "center",
    justifyContent: "center",
  },
  StartingDateText: {
    fontSize: rf(12),
    fontWeight: "400",
    textAlign: "center",
  },
  InputDateText: {
    borderBottomWidth: 2,
    borderColor: "#e5e5e5",
    marginTop: 10,
    fontSize: rf(12),
  },
  CalenderIconBody: {
    height: "70%",
    width: wp("15%"),
    alignItems: "center",
  },
  CalenderIcon: {
    height: hp("5%"),
    width: hp("5%"),
    justifyContent: "center",
    alignItems: "center",
  },
});
