import React from "react";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Image,
  ScrollView,
} from "react-native";
import Header from "../../components/Header";
import BottomMenu from "../../components/BottomMenu";
import DateSelectButton from "./components/DateSelectButton";
import DateSelecter from "./components/DateSelecter";
import { Calendar, CalendarList, Agenda } from "react-native-calendars";
export default function Calender({ navigation }) {
  return (
    <View style={styles.container}>
      <Header
        BackTxt="Back"
        Title="Calender"
        navigation={() => navigation.goBack()}
      />
      <ScrollView>
        <View style={styles.SelectPeriodTextBody}>
          <Text style={styles.SelectPeriodText}>Select Period</Text>
        </View>
        <View style={styles.DateSelecterWrapper}>
          <DateSelecter />
        </View>
        <View style={styles.CalenderWrapper}>
          <Calendar
            // Initially visible month. Default = Date()
            current={"2012-03-01"}
            // Month format in calendar title. Formatting values: http://arshaw.com/xdate/#Formatting
            monthFormat={"yyyy MM"}
            // Handler which gets executed when visible month changes in calendar. Default = undefined
            onMonthChange={(month) => {
              console.log("month changed", month);
            }}
            // Do not show days of other months in month page. Default = false
            hideExtraDays={true}
            // If firstDay=1 week starts from Monday. Note that dayNames and dayNamesShort should still start from Sunday
            firstDay={1}
            // Show week numbers to the left. Default = false
            showWeekNumbers={true}
            // Handler which gets executed when press arrow icon left. It receive a callback can go back month
            onPressArrowLeft={(subtractMonth) => subtractMonth()}
            // Handler which gets executed when press arrow icon right. It receive a callback can go next month
            onPressArrowRight={(addMonth) => addMonth()}
            // Disable left arrow. Default = false
            disableArrowLeft={true}
            // Disable right arrow. Default = false
            disableArrowRight={true}
            // Disable all touch events for disabled days. can be override with disableTouchEvent in markedDates
            disableAllTouchEventsForDisabledDays={true}
            // Enable the option to swipe between months. Default = false
            enableSwipeMonths={true}
          />
        </View>
        <View style={styles.SelectDateBtnWrapper}>
          <DateSelectButton title="Select specific date" />
          <DateSelectButton title="Select period" />
        </View>
        <TouchableOpacity style={styles.SearchEventBtn} onPress={()=>navigation.navigate("ThirdCalender")}>
          <Text style={styles.SeacrhEventText}>Search event</Text>
        </TouchableOpacity>
      </ScrollView>

      <BottomMenu navigation={navigation} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  DateSelecterWrapper: {
    height: hp("18%"),
    paddingHorizontal: wp("4%"),
  },
  SelectPeriodTextBody: {
    height: hp("4%"),
    paddingHorizontal: wp("4%"),
    justifyContent: "center",
  },
  SelectPeriodText: {
    fontSize: rf(14),
    fontWeight: "700",
  },
  CalenderWrapper: {
    height: hp("36%"),
    paddingHorizontal: wp("4%"),
  },
  SelectDateBtnWrapper: {
    height: hp("12%"),
    width: wp("100%"),
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
  },
  SearchEventBtn: {
    height: hp("5%"),
    width: wp("30%"),
    borderWidth: 2,
    borderColor: "#FF5623",
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
  },
  SeacrhEventText: {
    fontWeight: "700",
    fontSize: rf(12),
  },
});
