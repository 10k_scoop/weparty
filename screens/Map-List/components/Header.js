import React from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { AntDesign, Feather } from "@expo/vector-icons";

export default function Header(props) {
  return (
    <View style={styles.container}>
      <Text style={styles.Font}>Map</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("100%"),
    height: hp("10%"),
    backgroundColor: "#fff",
    justifyContent: "flex-end",
    alignItems: "center",
    marginBottom: "4%",
  },
  Font: {
    fontSize: rf(16),
    fontWeight: "700",
  },
});
