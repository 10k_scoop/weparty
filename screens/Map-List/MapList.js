import React from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import Header from "./components/Header";
import SearchBar from "./components/SearchBar";
import MapListCard from "./components/MapListCard";
import BottomMenu from "../../components/BottomMenu";
import { FontAwesome } from "@expo/vector-icons";

export default function MapList({ navigation }) {
  return (
    <View style={styles.container}>
      <Header />
      <SearchBar />
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.Wrapper}>
          <MapListCard
            img={require("../../assets/PartyPic.jpeg")}
            title="Lorem Ipsum"
            detail="Lorem Ipsum 1017 Amsterdam"
            distance="600m away"
          />
          <MapListCard
            img={require("../../assets/PartyPic.jpeg")}
            title="Lorem Ipsum"
            detail="Lorem Ipsum 1017 Amsterdam"
            distance="800m away"
          />
          <MapListCard
            img={require("../../assets/PartyPic.jpeg")}
            title="Lorem Ipsum"
            detail="900m away"
          />
          <MapListCard
            img={require("../../assets/PartyPic.jpeg")}
            title="Lorem Ipsum"
          />
          <MapListCard
            img={require("../../assets/PartyPic.jpeg")}
            title="Lorem Ipsum"
          />
          <MapListCard
            img={require("../../assets/PartyPic.jpeg")}
            title="Lorem Ipsum"
          />
          <MapListCard
            img={require("../../assets/PartyPic.jpeg")}
            title="Lorem Ipsum"
          />
          <MapListCard
            img={require("../../assets/PartyPic.jpeg")}
            title="Lorem Ipsum"
          />
          <MapListCard
            img={require("../../assets/PartyPic.jpeg")}
            title="Lorem Ipsum"
          />
        </View>
      </ScrollView>
      <TouchableOpacity
        style={styles.IconView}
        onPress={() => navigation.navigate("PointerMap")}
      >
        <FontAwesome name="map" size={rf(15)} color="#fff" />
      </TouchableOpacity>
      <BottomMenu navigation={navigation} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  Wrapper: {
    width: wp("100%"),
    alignItems: "center",
    marginBottom: "15%",
  },
  IconView: {
    width: hp("5%"),
    height: hp("5%"),
    borderRadius: 100,
    backgroundColor: "#FD581F",
    position: "absolute",
    bottom: hp("10%"),
    right: 15,
    alignItems: "center",
    justifyContent: "center",
  },
});
