import React from "react";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { StyleSheet, View, ScrollView } from "react-native";
import BottomMenu from "../../components/BottomMenu";
import Header from "./components/Header";
import MusicCategory from "./components/MusicCategory";
export default function Bars({ navigation }) {
  return (
    <View style={styles.container}>
      <Header onclick={() => navigation.navigate("FilterClub")} />
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.MusicFilterWrapper}>
          <MusicCategory Title="All" Icon />
          <MusicCategory Title="Bar 1" />
          <MusicCategory Title="Bar 2" Icon />
        </View>
      </ScrollView>
      <BottomMenu navigation={navigation} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  MusicFilterWrapper: {
    width: wp("100%"),
    alignItems: "center",
    marginBottom: "10%",
  },
});
