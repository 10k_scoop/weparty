import React from "react";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { StyleSheet, View, ScrollView } from "react-native";
import BottomMenu from "../../components/BottomMenu";
import Header from "./components/Header";
import MusicCategory from "./components/MusicCategory";
export default function FilterMusic({ navigation }) {
  return (
    <View style={styles.container}>
      <Header onclick={() => navigation.navigate("FilterClub")} />
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.MusicFilterWrapper}>
          <MusicCategory Title="All" Icon />
          <MusicCategory Title="90’s/00’s guilty pleasure" />
          <MusicCategory Title="90’s" Icon />
          <MusicCategory Title="Dance" />
          <MusicCategory Title="EDM" />
          <MusicCategory Title="Electronics" />
          <MusicCategory Title="Funk" Icon />
          <MusicCategory Title="Hip-Hop" />
          <MusicCategory Title="House" />
          <MusicCategory Title="Latin" />
          <MusicCategory Title="R&B" />
          <MusicCategory Title="Rap" />
          <MusicCategory Title="Soul" />
          <MusicCategory Title="Techno" />
          <MusicCategory Title="Trap" />
        </View>
      </ScrollView>
      <BottomMenu navigation={navigation} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  MusicFilterWrapper: {
    width: wp("100%"),
    alignItems: "center",
    marginBottom: "10%",
  },
});
