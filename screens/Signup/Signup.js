import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import ContinueBtn from "./components/ContinueBtn";
import { AntDesign } from "@expo/vector-icons";
import { Picker } from "@react-native-picker/picker";
export default function Signup({ navigation }) {
  const [isSelected, setIsSelected] = useState(false);
  const [gender, setGender] = useState("male");
  return (
    <View style={styles.container}>
      <View style={styles.layerHeader}></View>
      <View style={styles.layerCircle1}></View>
      <View style={styles.layerCircle2}></View>
      <View style={styles.layerCircle3}></View>
      <ScrollView>
        <View style={styles.cont}>
          <View style={styles.WelcomeTextWrapper}>
            <Text style={styles.WelcomeText}>Signup</Text>
          </View>
          <View style={styles.WrraperTextField}>
            <TextInput
              style={styles.TextField}
              placeholder="Name"
              placeholderTextColor="#222"
            />
          </View>
          <View style={styles.WrraperTextField}>
            <Picker
              selectedValue={gender}
              onValueChange={(itemValue, itemIndex) => setGender(itemValue)}
              itemStyle={styles.TextField}
            >
              <Picker.Item label="Gender" value={null} />
              <Picker.Item label="Male" value="Male" />
              <Picker.Item label="Female" value="Female" />
            </Picker>
          </View>
          <View style={styles.WrraperTextField}>
            <TextInput
              style={styles.TextField}
              placeholder="Age"
              placeholderTextColor="#222"
            />
          </View>
          <View style={styles.WrraperTextField}>
          <Picker
              selectedValue={gender}
              onValueChange={(itemValue, itemIndex) => setGender(itemValue)}
              itemStyle={styles.TextField}
            >
              <Picker.Item label="Single" value={null} />
              <Picker.Item label="Yes" value="yes" />
              <Picker.Item label="No" value="no" />
            </Picker>
          </View>
          <View style={styles.WrraperTextField}>
            <TextInput
              style={styles.TextField}
              placeholder="Email address"
              placeholderTextColor="#222"
            />
          </View>
          <View style={styles.WrraperTextField}>
            <TextInput
              style={styles.TextField}
              placeholder="Password"
              placeholderTextColor="#222"
              secureTextEntry
            />
          </View>
          <View style={styles.WrraperTextField}>
            <TextInput
              style={styles.TextField}
              placeholder="Confirm password"
              placeholderTextColor="#222"
              secureTextEntry
            />
          </View>
          <View style={styles.ConditionRow}>
            <TouchableOpacity
              style={styles.checkbox}
              onPress={() => setIsSelected(!isSelected)}
            >
              {isSelected && <AntDesign name="check" size={16} color="white" />}
            </TouchableOpacity>
            <Text style={styles.Font3}>I agree to the Terms & Conditions</Text>
          </View>
          {/* Buttons */}
          <View style={styles.ButtonsWrapper}>
            <ContinueBtn onClick={() => navigation.navigate("Home")} />
            <TouchableOpacity onPress={() => navigation.navigate("Login")}>
              <Text style={styles.bottomMsg}>
                Already have an account? click here to log in
              </Text>
            </TouchableOpacity>
          </View>
          {/* Buttons */}
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FF5623",
    alignItems: "center",
  },
  cont: {
    alignItems: "center",
    marginBottom: hp("5%"),
  },
  socialBtnWrapper: {
    flexDirection: "row",
  },
  WelcomeTextWrapper: {
    height: hp("28%"),
    justifyContent: "flex-end",
    paddingBottom: hp("5%"),
  },
  layerHeader: {
    width: wp("80%"),
    height: hp("23%"),
    backgroundColor: "white",
    position: "absolute",
    borderTopRightRadius: 1000,
    borderBottomLeftRadius: 1000,
    top: -hp("8%"),
    transform: [{ scaleX: 1.5 }],
  },
  layerCircle1: {
    width: wp("6%"),
    height: wp("6%"),
    backgroundColor: "#FF5623",
    position: "absolute",
    right: wp("7%"),
    top: hp("5%"),
    borderRadius: 100,
  },
  layerCircle2: {
    width: wp("3%"),
    height: wp("3%"),
    backgroundColor: "#FF5623",
    position: "absolute",
    right: wp("13.5%"),
    top: hp("4.8%"),
    borderRadius: 100,
  },
  layerCircle3: {
    width: wp("1.7%"),
    height: wp("1.7%"),
    backgroundColor: "#FF5623",
    position: "absolute",
    right: wp("17%"),
    top: hp("4.6%"),
    borderRadius: 100,
  },
  WelcomeText: {
    fontSize: rf(35),
    fontFamily: "MB",
    color: "#fff",
    letterSpacing: 1,
    textAlign: "center",
  },
  Font1: {
    fontSize: rf(24),
    color: "#fff",
    fontFamily: "MB",
    letterSpacing: 1,
  },
  WrraperTextField: {
    width: wp("90%"),
    height: hp("6%"),
    borderRadius: 10,
    backgroundColor: "#fff",
    marginBottom: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  TextField: {
    width: "100%",
    height: "100%",
    fontSize: rf(12),
    paddingHorizontal: wp("4%"),
    fontFamily: "MR",
    letterSpacing: 1,
    color: "#222",
  },
  layer: {
    width: "100%",
    height: "100%",
    backgroundColor: "#E5E5E5",
    opacity: 0.4,
    position: "absolute",
  },
  ConditionRow: {
    width: wp("90%"),
    paddingHorizontal: wp("2%"),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-start",
    marginVertical: hp("2%"),
  },
  Font3: {
    fontSize: rf(12),
    color: "#fff",
    fontFamily: "MR",
  },
  ButtonsWrapper: {
    width: wp("100%"),
    alignItems: "center",
    justifyContent: "flex-end",
    marginTop: hp("2%"),
  },
  checkbox: {
    width: 20,
    height: 20,
    borderWidth: 1,
    borderColor: "#fff",
    borderRadius: 5,
    marginRight: 10,
    alignItems: "center",
    justifyContent: "center",
  },
  bottomMsg: {
    fontSize: rf(12),
    color: "#fff",
    fontFamily: "MM",
    letterSpacing: 1,
  },
});
