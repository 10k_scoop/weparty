import React from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  Image,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { Feather } from "@expo/vector-icons";
import BottomMenu from "../../components/BottomMenu";
import Header from "./components/Header";
import Cards from "./components/Cards";

export default function Reason({ navigation }) {
  return (
    <View style={styles.container}>
      <Header onBack={() => navigation.goBack()} />
      <View style={styles.Wrapper}>
        <Cards txt="Impersonation and intellectual property" />
        <Cards txt="Privacy, Security, or Deception" />
        <Cards txt="Spam and minimum functionality" />
        <Cards txt="other" />
      </View>
      <BottomMenu navigation={navigation} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  Wrapper: {
    width: wp("100%"),
    alignItems: "flex-end",
    marginTop: "20%",
  },
});
