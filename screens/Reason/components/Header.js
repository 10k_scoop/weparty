import React from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";

export default function Header(props) {
  return (
    <View style={styles.container}>
      <View style={styles.DoneRow}>
        <TouchableOpacity onPress={props.onBack}>
          <Text style={styles.Font2}>Close</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.title}>
        <Text style={styles.Font1}>Reason</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("100%"),
    height: hp("10%"),
    flexDirection: "row",
  },
  title: {
    width: "85%",
    height: "100%",
    alignItems: "center",
    justifyContent: "flex-end",
    paddingVertical: "2%",
  },
  Font1: {
    fontSize: rf(16),
    fontWeight: "bold",
    marginRight: "20%",
    color: "#525151",
  },
  DoneRow: {
    width: "15%",
    height: "100%",
    alignItems: "center",
    justifyContent: "flex-end",
    paddingVertical: "2%",
  },
  Font2: {
    fontSize: rf(14),
    fontWeight: "bold",
    color: "#FF5623",
  },
});
