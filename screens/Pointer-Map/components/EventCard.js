import React from "react";
import { StyleSheet, Text, View, Image } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { AntDesign, Feather } from "@expo/vector-icons";

export default function EventCard(props) {
  return (
    <View style={styles.Wraaper}>
      <View style={styles.FirstRow}>
        <Text style={styles.Font}>WeParty</Text>
        <View style={styles.Img}>
          <Image
            source={require("../../../assets/EventPic.png")}
            resizeMode="cover"
            style={{ width: "100%", height: "100%" }}
          />
        </View>
      </View>
      <View style={styles.SecondRow}>
        <View>
          <Text style={styles.Font}>Lorem Ipsum</Text>
          <Text style={styles.Font2}>Lorem Ipsum 1017 Amsterdam </Text>
        </View>
        <View style={styles.Img}>
          <Image
            source={require("../../../assets/EventPic.png")}
            resizeMode="cover"
            style={{ width: "100%", height: "100%" }}
          />
        </View>
      </View>
      <View style={styles.ThirdRow}>
        <View>
          <Text style={styles.Font}>Lorem Ipsum</Text>
          <Text style={styles.Font2}>Lorem Ipsum 1017 Amsterdam </Text>
        </View>
        <View style={styles.Img}>
          <Image
            source={require("../../../assets/EventPic.png")}
            resizeMode="cover"
            style={{ width: "100%", height: "100%" }}
          />
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  Wraaper: {
    width: wp("100%"),
    height: hp("28%"),
    backgroundColor: "#fff",
    alignItems: "flex-end",
    justifyContent: "space-around",
    borderTopWidth: 1,
    borderColor: "#e5e5e5",
  },
  FirstRow: {
    width: "95%",
    height: "30%",
    borderBottomWidth: 2,
    borderColor: "#e5e5e5",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  Img: {
    width: "30%",
    height: "80%",
    borderRadius: 10,
    marginRight: 7,
    overflow: "hidden",
  },
  Font: {
    fontSize: rf(14),
    fontWeight: "bold",
    color: "#FF5623",
  },
  SecondRow: {
    width: "95%",
    height: "30%",
    borderBottomWidth: 2,
    borderColor: "#e5e5e5",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  ThirdRow: {
    width: "95%",
    height: "30%",
    borderColor: "#e5e5e5",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  Font2: {
    fontSize: rf(12),
    fontWeight: "400",
    color: "grey",
  },
});
