import React from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { AntDesign, Feather } from "@expo/vector-icons";

export default function Header(props) {
  return (
    <View style={styles.container}>
      <View style={styles.title}>
        <Text style={styles.Font}>Map</Text>
      </View>
      <View style={styles.IconsView}>
        <TouchableOpacity>
          <Feather name="calendar" size={rf(20)} color="#FD581F" />
        </TouchableOpacity>
        <TouchableOpacity style={{ marginLeft: 5 }} onPress={props.onAdd}>
          <AntDesign name="filter" size={rf(20)} color="#FD581F" />
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("100%"),
    height: hp("10%"),
    backgroundColor: "#fff",
    alignItems: "flex-end",
    paddingVertical: 10,
    flexDirection: "row",
  },
  IconsView: {
    flexDirection: "row",
  },
  Font: {
    fontSize: rf(16),
    fontWeight: "700",
    left: "10%",
  },
  title: {
    width: "82%",
    height: "100%",
    alignItems: "center",
    justifyContent: "flex-end",
  },
});
