import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import Header from "./components/Header";
import ContinueBtn from "./components/ContinueBtn";
import SocialLoginBtns from "./components/SocialLoginBtns";
import { AntDesign } from "@expo/vector-icons";

export default function Login({ navigation }) {
  const [isSelected, setIsSelected] = useState(false);

  return (
    <View style={styles.container}>
      <View style={styles.layerHeader}></View>
      <View style={styles.layerCircle1}></View>
      <View style={styles.layerCircle2}></View>
      <View style={styles.layerCircle3}></View>
      <View style={styles.WelcomeTextWrapper}>
        <View style={styles.logo}>
          <Image
            source={require("../../assets/wplogo.png")}
            resizeMode="contain"
            style={{ width: "100%", height: "100%" }}
          />
        </View>
        <Text style={styles.WelcomeText}>Login</Text>
      </View>
      <View style={styles.WrraperTextField}>
        {/* <View style={styles.layer}></View> */}
        <TextInput
          style={styles.TextField}
          placeholder="Email address"
          placeholderTextColor="#222"
        />
      </View>
      <View style={styles.WrraperTextField}>
        {/* <View style={styles.layer}></View> */}
        <TextInput
          style={styles.TextField}
          placeholder="Password"
          placeholderTextColor="#222"
          secureTextEntry
        />
      </View>
      <View style={styles.ConditionRow}>
        <TouchableOpacity
          style={styles.checkbox}
          onPress={() => setIsSelected(!isSelected)}
        >
          {isSelected && <AntDesign name="check" size={16} color="white" />}
        </TouchableOpacity>
        <Text style={styles.Font3}>I agree to the Terms & Conditions</Text>
      </View>
      {/* Buttons */}
      <View style={styles.ButtonsWrapper}>
        <ContinueBtn onClick={() => navigation.navigate("Home")} />
        <View style={styles.socialBtnWrapper}>
          <SocialLoginBtns icon="gmail" />
          <SocialLoginBtns icon="facebook" />
          <SocialLoginBtns icon="apple" />
        </View>
        <TouchableOpacity onPress={() => navigation.navigate("Signup")}>
          <Text style={styles.bottomMsg}>
            Don't have an account yet? click here to Signup
          </Text>
        </TouchableOpacity>
      </View>
      {/* Buttons */}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FF5623",
    alignItems: "center",
  },
  logo: {
    width: wp("20%"),
    height: hp("10%"),
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  socialBtnWrapper: {
    flexDirection: "row",
  },
  WelcomeTextWrapper: {
    height: hp("40%"),
    justifyContent: "flex-end",
    paddingBottom: hp("5%"),
    alignItems: "center",
  },
  layerHeader: {
    width: wp("80%"),
    height: hp("23%"),
    backgroundColor: "white",
    position: "absolute",
    borderTopRightRadius: 1000,
    borderBottomLeftRadius: 1000,
    top: -hp("8%"),
    transform: [{ scaleX: 1.5 }],
  },
  layerCircle1: {
    width: wp("6%"),
    height: wp("6%"),
    backgroundColor: "#FF5623",
    position: "absolute",
    right: wp("7%"),
    top: hp("5%"),
    borderRadius: 100,
  },
  layerCircle2: {
    width: wp("3%"),
    height: wp("3%"),
    backgroundColor: "#FF5623",
    position: "absolute",
    right: wp("13.5%"),
    top: hp("4.8%"),
    borderRadius: 100,
  },
  layerCircle3: {
    width: wp("1.7%"),
    height: wp("1.7%"),
    backgroundColor: "#FF5623",
    position: "absolute",
    right: wp("17%"),
    top: hp("4.6%"),
    borderRadius: 100,
  },
  WelcomeText: {
    fontSize: rf(35),
    fontFamily: "MB",
    color: "#fff",
    letterSpacing: 1,
    textAlign: "center",
  },
  Font1: {
    fontSize: rf(24),
    color: "#fff",
    fontFamily: "MB",
    letterSpacing: 1,
  },
  WrraperTextField: {
    width: wp("90%"),
    height: hp("7%"),
    borderRadius: 10,
    backgroundColor: "#fff",
    marginBottom: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  TextField: {
    width: "100%",
    height: "100%",
    fontSize: rf(12),
    paddingHorizontal: wp("4%"),
    fontFamily: "MR",
    letterSpacing: 1,
    color: "#222",
  },
  layer: {
    width: "100%",
    height: "100%",
    backgroundColor: "#E5E5E5",
    opacity: 0.4,
    position: "absolute",
  },
  ConditionRow: {
    width: wp("90%"),
    height: hp("6%"),
    paddingHorizontal: wp("2%"),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-start",
  },
  Font3: {
    fontSize: rf(12),
    color: "#fff",
    fontFamily: "MR",
  },
  ButtonsWrapper: {
    width: wp("100%"),
    alignItems: "center",
    justifyContent: "flex-end",
    marginTop: hp("5%"),
  },
  checkbox: {
    width: 20,
    height: 20,
    borderWidth: 1,
    borderColor: "#fff",
    borderRadius: 5,
    marginRight: 10,
    alignItems: "center",
    justifyContent: "center",
  },
  bottomMsg: {
    fontSize: rf(12),
    color: "#fff",
    marginTop: hp("8%"),
    fontFamily: "MM",
    letterSpacing: 1,
  },
});
