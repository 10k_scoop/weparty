import React from "react";
import { StyleSheet, Text, View } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";

export default function Header() {
  return (
    <View style={styles.container}>
      <Text style={{ fontSize: rf(16), fontWeight: "700" }}>Login</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("100%"),
    height: hp("10%"),
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "flex-end",
    paddingVertical: 10,
  },
});
