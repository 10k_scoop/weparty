import React from "react";
import { StyleSheet, Text, View, TouchableOpacity, Image } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { MaterialCommunityIcons } from "@expo/vector-icons";

export default function SocialLoginBtns(props) {
  return (
    <TouchableOpacity style={styles.container}>
      <View style={styles.layer}></View>
      <MaterialCommunityIcons name={props.icon} size={wp('8%')} color="white" />
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("12%"),
    height: wp("12%"),
    borderRadius: 1000,
    alignItems: "center",
    justifyContent: "center",
    overflow: "hidden",
    flexDirection: "row",
    marginHorizontal:10
  },
  layer: {
    width: "100%",
    height: "100%",
    backgroundColor: "#E5E5E5",
    opacity: 0.4,
    position: "absolute",
  },
  Font: {
    fontSize: rf(14),
    fontWeight: "bold",
  },
});
