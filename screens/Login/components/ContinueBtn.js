import React from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";

export default function ContinueBtn(props) {
  return (
    <TouchableOpacity style={styles.container} onPress={props.onClick}>
      <View style={styles.layer}></View>
      <Text style={{ fontSize: rf(14), fontWeight: "bold", color: "#222",fontFamily:'MM',letterSpacing:1 }}>
        Continue
      </Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("90%"),
    height: hp("6%"),
    borderRadius: 10,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#fff",
    marginBottom: hp('5%'),
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  layer: {
    width: "100%",
    height: "100%",
    backgroundColor: "#E5E5E5",
    opacity: 0.4,
    position: "absolute",
  },
});
