import React from "react";
import { StyleSheet, Text, View, Image, ScrollView } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import BottomMenu from "../../components/BottomMenu";
import Header from "./components/Header";
import PartyCards from "./components/Partycards";

export default function Favourites({ navigation }) {
  return (
    <View style={styles.container}>
      <Header onBackClick={() => navigation.goBack()} />
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.Wrapper}>
          <PartyCards date="November 14, 2021" />
          <PartyCards date="october 14, 2021" />
        </View>
      </ScrollView>
      <BottomMenu navigation={navigation} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  Wrapper: {
    width: wp("100%"),
    alignItems: "center",
    marginTop: "10%",
    marginBottom: "10%",
  },
});
