import React from "react";
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  TouchableOpacity,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { Ionicons, MaterialCommunityIcons } from "@expo/vector-icons";

export default function PartyCards(props) {
  return (
    <View style={styles.container}>
      <Text style={styles.DateFont}>{props.date}</Text>
      <View style={styles.Card}>
        <View style={styles.FirstRow}>
          <ImageBackground
            source={require("../../../assets/PartyPic.jpeg")}
            style={{ width: "100%", height: "100%" }}
            resizeMode="cover"
          >
            <View style={styles.Inner}>
              <TouchableOpacity style={styles.Ticket}>
                <Text style={styles.TicketFont}>Get Tickets</Text>
              </TouchableOpacity>
              <TouchableOpacity>
                <Ionicons name="heart" size={rf(22)} color="#FD581F" />
              </TouchableOpacity>
            </View>
          </ImageBackground>
        </View>
        <View style={styles.SecondRow}>
          <View>
            <Text style={styles.Font1}>Lorem Ipsum</Text>
            <Text style={styles.Font2}>Lorem Ipsum is simply dummy text</Text>
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("95%"),
    height: hp("32%"),
    justifyContent: "space-around",
    marginBottom: "7%",
  },
  DateFont: {
    fontSize: rf(14),
    fontWeight: "700",
    marginLeft: 10,
  },
  Card: {
    width: "100%",
    height: "85%",
    borderRadius: 10,
    overflow: "hidden",
    backgroundColor: "#fff",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  FirstRow: {
    width: "100%",
    height: "70%",
    backgroundColor: "grey",
  },
  Inner: {
    width: "100%",
    height: "40%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingHorizontal: "2%",
  },
  Ticket: {
    width: "38%",
    height: "60%",
    backgroundColor: "#FD581F",
    borderRadius: 8,
    justifyContent: "center",
    alignItems: "center",
  },
  TicketFont: {
    fontSize: rf(13),
    fontWeight: "bold",
    color: "#fff",
  },
  SecondRow: {
    width: "100%",
    height: "30%",
    justifyContent: "center",
    paddingHorizontal: "4%",
  },
  Font1: {
    fontSize: rf(14),
    fontWeight: "bold",
    color: "#FF5623",
  },
  Font2: {
    fontSize: rf(12),
    fontWeight: "400",
    color: "grey",
  },
  Inner2: {
    width: "10%",
    height: "100%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
});
