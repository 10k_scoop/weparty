import React from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { AntDesign, Feather } from "@expo/vector-icons";

export default function Header(props) {
  return (
    <View style={styles.container}>
      <View style={styles.Icon}>
        <TouchableOpacity onPress={props.onBackClick}>
          <AntDesign name="left" size={rf(18)} color="#FD581F" />
        </TouchableOpacity>
        <Text style={styles.Font1}>Back</Text>
      </View>
      <View style={styles.title}>
        <Text style={styles.Font2}>Favourites</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("100%"),
    height: hp("10%"),
    flexDirection: "row",
  },
  Icon: {
    width: "15%",
    height: "100%",
    flexDirection: "row",
    alignItems: "flex-end",
    justifyContent: "space-around",
    paddingVertical: "2%",
  },
  Font1: {
    fontSize: rf(14),
    fontWeight: "bold",
    color: "#FD581F",
  },
  title: {
    width: "85%",
    height: "100%",
    alignItems: "center",
    justifyContent: "flex-end",
    paddingVertical: "2%",
  },
  Font2: {
    fontSize: rf(16),
    fontWeight: "bold",
    marginRight: "15%",
  },
});
