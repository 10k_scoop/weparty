import React from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import Header from "./components/Header";
import SearchBar from "./components/SearchBar";
import BottomMenu from "../../components/BottomMenu";
import { Entypo, FontAwesome5, Ionicons } from "@expo/vector-icons";
import MapView from "react-native-maps";
export default function Map({ navigation }) {
  return (
    <View style={styles.container}>
      <Header onFilterPress={() => navigation.navigate("Filter")} onCalendarPress={() => navigation.navigate("Calender")} />
      <SearchBar />
      <View style={styles.mapContainer}>
        <MapView
          style={styles.map}
          region={{
            latitude: 52.377956,
            longitude: 4.88969,
            latitudeDelta: 0.015,
            longitudeDelta: 0.0121,
          }}
        />
      </View>
      <View style={styles.MapView}></View>
      <TouchableOpacity style={styles.naviIcon}>
        <FontAwesome5 name="location-arrow" size={rf(20)} color="#FD581F" />
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.listIcon}
        onPress={() => navigation.navigate("MapList")}
      >
        <Entypo name="list" size={rf(20)} color="#FD581F" />
      </TouchableOpacity>
      <BottomMenu navigation={navigation} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  MapView: {
    width: wp("100%"),
    height: hp("100%"),
  },
  naviIcon: {
    position: "absolute",
    bottom: wp("39%"),
    right: 15,
    width: wp("15%"),
    height: wp("15%"),
    backgroundColor: "white",
    borderRadius: 100,
    alignItems: "center",
    justifyContent: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  listIcon: {
    position: "absolute",
    bottom: wp("20%"),
    right: 15,
    width: wp("15%"),
    height: wp("15%"),
    backgroundColor: "white",
    borderRadius: 100,
    alignItems: "center",
    justifyContent: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  map: {
    width: wp("100%"),
    height: hp("100%"),
  },
});
