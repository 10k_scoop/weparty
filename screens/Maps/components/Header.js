import React from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { AntDesign, Feather } from "@expo/vector-icons";

export default function Header(props) {
  return (
    <View style={styles.container}>
      <View style={styles.title}>
        <Text style={styles.Font}>Map</Text>
      </View>
      <View style={styles.IconsView}>
        <TouchableOpacity onPress={props.onCalendarPress}>
          <Feather name="calendar" size={rf(23)} color="#FD581F" />
        </TouchableOpacity>
        <TouchableOpacity style={{ marginLeft: 15 }} onPress={props.onFilterPress}>
          <AntDesign name="filter" size={rf(23)} color="#FD581F" />
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("100%"),
    height: hp("10%"),
    backgroundColor: "#fff",
    alignItems: "flex-end",
    paddingVertical: 10,
    flexDirection: "row",
  },
  IconsView: {
    flexDirection: "row",
    right:15,
  },
  Font: {
    fontSize: rf(16),
    fontFamily:'MB',
    left: "10%",
    letterSpacing:0.8
  },
  title: {
    width: "82%",
    height: "100%",
    alignItems: "center",
    justifyContent: "flex-end",
  },
});
