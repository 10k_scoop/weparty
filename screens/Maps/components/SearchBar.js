import React from "react";
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { Feather } from "@expo/vector-icons";

export default function SearchBar(props) {
  return (
    <View style={styles.container}>
      <View style={styles.Layer}></View>
      <TouchableOpacity style={styles.searchIcon}>
        <Feather name="search" size={rf(20)} color="black" />
      </TouchableOpacity>
      <TextInput
        style={styles.TextField}
        placeholder="Find your party/club/DJ/genre"
        placeholderTextColor="#A79A9A"
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("95%"),
    height: hp("6%"),
    borderRadius: 10,
    flexDirection: "row",
    overflow: "hidden",
    alignItems: "center",
    marginTop: 10,
    marginBottom: 5,
  },
  Layer: {
    width: "100%",
    height: "100%",
    backgroundColor: "#E5E5E5",
    opacity: 0.4,
    position: "absolute",
  },
  TextField: {
    width: "90%",
    height: "100%",
    paddingHorizontal: 5,
    fontSize: rf(12),
    fontFamily:'MM',
    letterSpacing:1
  },
  searchIcon: {
    width: "10%",
    height: "100%",
    justifyContent: "center",
    alignItems: "center",
  },
});
