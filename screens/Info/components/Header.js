import React from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";

export default function Header(props) {
  return (
    <View style={styles.container}>
      <View style={styles.title}>
        <Text style={styles.Font1}>Info</Text>
      </View>
      <View style={styles.DoneRow}>
        <TouchableOpacity>
          <Text style={styles.Font2}>Done</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("100%"),
    height: hp("10%"),
    flexDirection: "row",
  },
  title: {
    width: "85%",
    height: "100%",
    alignItems: "center",
    justifyContent: "flex-end",
    paddingVertical: "2%",
  },
  Font1: {
    fontSize: rf(16),
    fontWeight: "bold",
    marginLeft: "20%",
  },
  DoneRow: {
    width: "15%",
    height: "100%",
    alignItems: "center",
    justifyContent: "flex-end",
    paddingVertical: "2%",
  },
  Font2: {
    fontSize: rf(14),
    fontWeight: "bold",
    color: "#FF5623",
  },
});
