import React from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { AntDesign } from "@expo/vector-icons";

export default function InfoCard(props) {
  return (
    <View style={styles.Container}>
      <Text style={styles.SupportFont}>{props.title}</Text>
      <View style={styles.Icon}>
        <Text style={[styles.SupportFont, { color: "#FF5623" }]}>
          {props.detail}
        </Text>
        <TouchableOpacity onPress={props.onAdd}>
          <AntDesign name="right" size={rf(18)} color="#FF5623" />
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  Container: {
    width: wp("90%"),
    height: hp("5%"),
    borderBottomWidth: 2,
    borderColor: "#e5e5e5",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  SupportFont: {
    fontSize: rf(14),
    fontWeight: "400",
    color: "#525151",
    marginRight: 5,
  },
  Icon: {
    flexDirection: "row",
  },
});
