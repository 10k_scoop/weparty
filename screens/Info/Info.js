import React from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  Image,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { Feather } from "@expo/vector-icons";
import BottomMenu from "../../components/BottomMenu";
import Header from "./components/Header";
import InfoCard from "./components/InfoCard";

export default function Info({ navigation }) {
  return (
    <View style={styles.container}>
      <Header />
      <View style={styles.Title}>
        <Image
          source={require("../../assets/InfoPic.png")}
          style={{ width: "15%", height: "40%" }}
          resizeMode="contain"
        />
        <Text style={styles.titleFont}>WeParty</Text>
      </View>
      <View style={styles.AuthorRow}>
        <Text style={styles.AuthorFont}>Author</Text>
        <Text style={styles.AuthorFont}>WePartyGroup</Text>
      </View>
      <View style={styles.ShareRow}>
        <Text style={styles.ShareFont}>Share</Text>
        <Text style={[styles.ShareFont, { color: "#FF5623" }]}>
          wepartynow.com/
        </Text>
      </View>
      {/* InfoCards */}
      <InfoCard
        title="Support"
        detail="Send feedback"
        onAdd={() => navigation.navigate("Support")}
      />
      <InfoCard
        title="Security"
        detail="Report this app"
        onAdd={() => navigation.navigate("Security")}
      />
      <InfoCard
        title="FAQs"
        detail="View"
        onAdd={() => navigation.navigate("FAQ")}
      />
      {/* InfoCards */}
      <View style={styles.QRScane}>
        <Image
          source={require("../../assets/QRPic.png")}
          resizeMode="center"
          style={{ width: "40%", height: "60%" }}
        />
        <Text style={styles.ScanFont}>Scan Code</Text>
      </View>
      <BottomMenu navigation={navigation} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  Title: {
    width: wp("90%"),
    height: hp("22%"),
    alignItems: "center",
    justifyContent: "center",
    marginBottom: 10,
  },
  titleFont: {
    fontSize: rf(20),
    fontWeight: "bold",
    color: "#FF5623",
  },
  AuthorRow: {
    width: wp("90%"),
    height: hp("5%"),
    borderBottomWidth: 2,
    borderColor: "#e5e5e5",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  AuthorFont: {
    fontSize: rf(14),
    fontWeight: "400",
    color: "#525151",
    marginRight: "6%",
  },
  ShareRow: {
    width: wp("90%"),
    height: hp("5%"),
    borderBottomWidth: 2,
    borderColor: "#e5e5e5",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  ShareFont: {
    fontSize: rf(14),
    fontWeight: "400",
    color: "#525151",
    marginRight: "6%",
  },
  QRScane: {
    width: wp("90%"),
    height: hp("30%"),
    alignItems: "center",
    justifyContent: "center",
  },
  ScanFont: {
    fontSize: rf(14),
    fontWeight: "400",
    marginTop: 10,
  },
});
