import React, { useState } from "react";
import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
export default function onBoarding({ navigation }) {
  return (
    <View style={styles.container}>
      <View style={styles.layer}></View>
      <View style={styles.layerCircle1}></View>
      <View style={styles.layerCircle2}></View>
      <View style={styles.layerCircle3}></View>
      <View style={styles.cont}>
        <View style={styles.WelcomeTextWrapper}>
          <Text style={styles.WelcomeText}>Welcome</Text>
        </View>
        <View style={styles.imageWrapper}>
          <Image
            source={require("../../assets/onBoarding.png")}
            resizeMode="contain"
            style={{ width: "100%", height: "100%" }}
          />
        </View>
        <View style={styles.actionButtonWrappper}>
          <TouchableOpacity
            style={styles.actionButtons}
            onPress={() => navigation.navigate("Login")}
          >
            <Text style={styles.actionButtonsText}>Login</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.actionButtons}
            onPress={() => navigation.navigate("Signup")}
          >
            <Text style={styles.actionButtonsText}>Signup</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FF5623",
    alignItems: "center",
  },
  layer: {
    width: wp("80%"),
    height: hp("23%"),
    backgroundColor: "white",
    position: "absolute",
    borderTopRightRadius: 1000,
    borderBottomLeftRadius: 1000,
    top: -hp("8%"),
    transform: [{ scaleX: 1.5 }],
  },
  layerCircle1: {
    width: wp("6%"),
    height: wp("6%"),
    backgroundColor: "#FF5623",
    position: "absolute",
    right: wp("7%"),
    top: hp("5%"),
    borderRadius: 100,
  },
  layerCircle2: {
    width: wp("3%"),
    height: wp("3%"),
    backgroundColor: "#FF5623",
    position: "absolute",
    right: wp("13.5%"),
    top: hp("4.8%"),
    borderRadius: 100,
  },
  layerCircle3: {
    width: wp("1.7%"),
    height: wp("1.7%"),
    backgroundColor: "#FF5623",
    position: "absolute",
    right: wp("17%"),
    top: hp("4.6%"),
    borderRadius: 100,
  },
  imageWrapper: {
    flex: 0.7,
    justifyContent: "center",
  },
  WelcomeTextWrapper: {
    flex: 0.5,
    justifyContent: "flex-end",
  },
  actionButtonWrappper: {
    flex: 0.3,
    justifyContent: "center",
    marginBottom: hp("3%"),
  },
  WelcomeText: {
    fontSize: rf(35),
    fontFamily: "MB",
    color: "#fff",
    letterSpacing: 1,
    textAlign: "center",
  },
  actionButtons: {
    width: wp("90%"),
    height: hp("6%"),
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 10,
    marginVertical: wp("2%"),
    backgroundColor: "white",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  actionButtonsText: {
    fontSize: rf(14),
    fontFamily: "MB",
    color: "#222",
    letterSpacing: 1,
  },
});
