import React from "react";
import { StyleSheet, Text, View, TouchableOpacity, Image } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { AntDesign, Feather } from "@expo/vector-icons";

export default function MapListCard(props) {
  return (
    <TouchableOpacity style={styles.container} onPress={props.onPress}>
      <View>
        <Text style={styles.Font1}>{props.title}</Text>
        <Text style={styles.Font2}>{props.detail}</Text>
        <Text style={styles.Font2}>{props.distance}</Text>
      </View>
      <View style={styles.Img}>
        <Image
          source={props.img}
          style={{ width: "100%", height: "100%" }}
          resizeMode="cover"
        />
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("90%"),
    height: hp("10%"),
    borderBottomWidth: 2,
    borderColor: "#e5e5e5",
    flexDirection: "row",
    justifyContent: "space-between",
    marginBottom: "5%",
    alignItems: "center",
  },
  Font1: {
    fontSize: rf(14),
    fontWeight: "700",
  },
  Font2: {
    fontSize: rf(12),
    fontWeight: "400",
  },
  Img: {
    width: "30%",
    height: "90%",
    borderWidth: 1,
    borderRadius: 10,
    overflow: "hidden",
  },
});
