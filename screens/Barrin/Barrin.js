import React from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import Header from "./components/Header";
import SearchBar from "./components/SearchBar";
import MapListCard from "./components/MapListCard";
import BottomMenu from "../../components/BottomMenu";
import { FontAwesome } from "@expo/vector-icons";

export default function Barrin({ navigation }) {
  return (
    <View style={styles.container}>
      <Header />
      <SearchBar />
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.Wrapper}>
          <MapListCard
            img={require("../../assets/PartyPic.jpeg")}
            title="Bar 1"
            detail="Opening hours "
            distance="9:00 am - 5:00 am"
            onPress={() => navigation.navigate("Event",{type:'bars'})}
          />
          <MapListCard
            img={require("../../assets/PartyPic.jpeg")}
            title="Bar 1"
            detail="Opening hours "
            distance="9:00 am - 5:00 am"
          />
          <MapListCard
            img={require("../../assets/PartyPic.jpeg")}
            title="Bar 1"
            detail="Opening hours "
            distance="9:00 am - 5:00 am"
          />

          <MapListCard
            img={require("../../assets/PartyPic.jpeg")}
            title="Bar 1"
            detail="Opening hours "
            distance="9:00 am - 5:00 am"
          />
          <MapListCard
            img={require("../../assets/PartyPic.jpeg")}
            title="Bar 1"
            detail="Opening hours "
            distance="9:00 am - 5:00 am"
          />
          <MapListCard
            img={require("../../assets/PartyPic.jpeg")}
            title="Bar 1"
            detail="Opening hours "
            distance="9:00 am - 5:00 am"
          />
          <MapListCard
            img={require("../../assets/PartyPic.jpeg")}
            title="Bar 1"
            detail="Opening hours "
            distance="9:00 am - 5:00 am"
          />
        </View>
      </ScrollView>
      <TouchableOpacity
        style={styles.IconView}
        onPress={() => navigation.navigate("PointerMap")}
      >
        <FontAwesome name="map" size={rf(15)} color="#fff" />
      </TouchableOpacity>
      <BottomMenu navigation={navigation} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  Wrapper: {
    width: wp("100%"),
    alignItems: "center",
    marginBottom: "15%",
  },
  IconView: {
    width: hp("5%"),
    height: hp("5%"),
    borderRadius: 100,
    backgroundColor: "#FD581F",
    position: "absolute",
    bottom: hp("10%"),
    right: 15,
    alignItems: "center",
    justifyContent: "center",
  },
});
