import React from "react";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { StyleSheet, Text, View } from "react-native";
import Header from "../../components/Header";
import ProfileDetails from "./component/ProfileDetails";
import PartyDetails from "./component/PartyDetails";
import BottomMenu from "../../components/BottomMenu";
export default function Profile({ navigation }) {
  return (
    <View style={styles.container}>
      <Header back Title="Profile" navigation={() => navigation.goBack()} />
      <View style={styles.ProfileDetailWrapper}>
        <ProfileDetails
          click={() => navigation.navigate("EditProfile")}
          Image={require("../../assets/ProfilePhoto.png")}
        />
      </View>
      <View style={styles.VisitTextWrapper}>
        <Text style={styles.VisitText}>Last Visited</Text>
      </View>
      <View style={styles.PartyDetailsWrapper}>
        <PartyDetails
          Title="WeParty"
          Image={require("../../assets/PartyPic.png")}
        />
        <PartyDetails
          Title="Lorem Ipsum"
          Image={require("../../assets/PartyPic.png")}
          PartyTxt="Lorem Ipsum 1017 Amsterdam"
        />
        <PartyDetails
          Title="Lorem Ipsum"
          Image={require("../../assets/PartyPic.png")}
          PartyTxt="Lorem Ipsum 1017 Amsterdam"
        />
      </View>
      <BottomMenu navigation={navigation} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  ProfileDetailWrapper: {
    height: hp("34%"),
    paddingHorizontal: wp("4%"),
  },
  VisitTextWrapper: {
    height: hp("8%"),
    paddingHorizontal: wp("6%"),
    justifyContent: "center",
  },
  VisitText: {
    fontSize: rf(14),
    fontFamily: "MB",
    letterSpacing:0.7
  },
  PartyDetailsWrapper: {
    paddingHorizontal: wp("6%"),
  },
});
