import React from "react";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native";
export default function PartyDetails(props) {
  return (
    <View style={styles.PartyDetailBody}>
      <View style={styles.TitleTextBody}>
        <Text style={styles.TitleText}>{props.Title}</Text>
        <Text style={styles.DetailText}>{props.PartyTxt}</Text>
      </View>
      <TouchableOpacity style={styles.PartyPicBody}>
        <Image
          source={props.Image}
          style={{ height: "100%", width: "100%" }}
          resizeMode="contain"
        />
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  PartyDetailBody: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    borderBottomWidth: 1,
    borderBottomColor: "#E5E5E5",
    height: hp("9%"),
  },
  TitleTextBody: {
    height: hp("6%"),
    justifyContent: "center",
  },
  TitleText: {
    fontFamily: "MB",
    letterSpacing:0.7,
    fontSize: rf(12),
    color: "#FF5623",
  },
  DetailText: {
    fontFamily: "MM",
    letterSpacing:0.7,
    fontSize: rf(12),
    color: "#cdcdcd",
    top:10
  },
  PartyPicBody: {
    height: hp("6%"),
    width: "30%",
    borderRadius: 10,
    overflow: "hidden",
  },
});
