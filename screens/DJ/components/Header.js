import React from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { AntDesign, Feather } from "@expo/vector-icons";

export default function Header(props) {
  return (
    <View style={styles.container}>
      <View style={styles.IconView}>
        <TouchableOpacity
          onPress={props.onBackClick}
          style={{ flexDirection: "row", alignItems: "center" }}
        >
          <AntDesign name="left" size={rf(14)} color="#FD581F" />
          <Text
            style={{
              fontFamily: "MB",
              fontSize: rf(14),
              fontWeight: "bold",
              color: "#FD581F",
              left: 5,
            }}
          >
            Back
          </Text>
        </TouchableOpacity>
      </View>
      <View style={styles.title}>
        <Text style={styles.titlefont1}>DJ MIKE</Text>
        <Text style={styles.titlefont2}></Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("100%"),
    height: hp("13%"),
    flexDirection: "row",
    paddingBottom: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 2,
    backgroundColor: "white",
  },
  IconView: {
    width: "15%",
    height: "100%",
    justifyContent: "space-around",
    alignItems: "flex-end",
    flexDirection: "row",
    paddingVertical: "5%",
  },
  title: {
    width: "85%",
    height: "100%",
    alignItems: "center",
    justifyContent: "flex-end",
  },
  titlefont1: {
    fontSize: rf(16),
    fontFamily: "MB",
    right: 20,
    marginBottom:5
  },
  titlefont2: {
    fontSize: rf(12),
    fontFamily:'MM',
    right: 20,
    color: "#424242",
  },
});
