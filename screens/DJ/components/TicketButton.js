import React from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { Foundation } from "@expo/vector-icons";

export default function TicketButton(props) {
  return (
    <TouchableOpacity style={styles.container}>
      <Foundation name="ticket" size={24} color="#fff" />
      <Text style={styles.Font}>Get Your Tickets</Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("90%"),
    height: hp("7%"),
    flexDirection: "row",
    backgroundColor: "#FD581F",
    borderRadius: 10,
    alignItems: "center",
    justifyContent: "center",
    marginBottom: 10,
  },
  Font: {
    fontSize: rf(14),
    fontFamily:'MB',
    color: "#fff",
    marginLeft: 10,
  },
});
