import React from "react";
import { StyleSheet, Text, View, TouchableOpacity, Image } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { AntDesign, MaterialCommunityIcons } from "@expo/vector-icons";

export default function FavouritesCard(props) {
  return (
    <View style={styles.container}>
      <View style={styles.Inner2}>
        <MaterialCommunityIcons name="account" size={rf(22)} color="#FF5623" />
        <Text style={styles.Font1}>4</Text>
        <View style={styles.singleDiv}>
          <Image
            source={require("../../../assets/singleLogo.png")}
            resizeMode="contain"
            style={{ width: '100%', height: '100%' }}
          />
          <Text style={styles.Font1}>4</Text>
        </View>
      </View>


      <View style={styles.icons}>
        <TouchableOpacity style={styles.Icon}>
          <AntDesign name="hearto" size={rf(22)} color="#FD581F" />
        </TouchableOpacity>
        <TouchableOpacity>
          <MaterialCommunityIcons
            name="instagram"
            size={rf(22)}
            color="#FD581F"
          />
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("90%"),
    height: hp("8%"),
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: "3%",
  },
  icons: {
    flexDirection: "row",
  },
  Icon: {
    marginRight: 10,
  },
  Font2: {
    fontSize: rf(12),
    fontFamily: "MR",
    color: "grey",
  },
  Inner2: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-start",
  },
  singleDiv: {
    width: 40,
    height: 40,
    flexDirection:'row',
    marginLeft:15,
    justifyContent:'center',
    alignItems:'center'
  }
});
