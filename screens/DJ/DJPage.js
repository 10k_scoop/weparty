import React from "react";
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    ScrollView,
    ImageBackground,
    Button,
} from "react-native";
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import Header from "./components/Header";
import FavouritesCard from "./components/FavouritesCard";
import TicketButton from "./components/TicketButton";
import ShareButton from "./components/ShareButton";
import DetailCard from "./components/DetailCard";
import BottomMenu from "../../components/BottomMenu";
import { Video, AVPlaybackStatus } from "expo-av";
import MapView from "react-native-maps";
import EventCard from "../Home/components/EventCard";

export default function DJPage({ navigation }) {
    const video = React.useRef(null);
    const [status, setStatus] = React.useState({});

    return (
        <View style={styles.container}>
            <Header onBackClick={() => navigation.goBack()} />
            <ScrollView>
                <View style={styles.wrapper}>
                    <View style={styles.Img}>
                        <ImageBackground
                            source={require("../../assets/PartyPic.jpeg")}
                            style={{ width: "100%", height: "100%" }}
                            resizeMode="cover"
                        ></ImageBackground>
                    </View>
                    <Text style={[styles.Font1, { marginVertical: hp('2%') }]}>DJ Mike</Text>
                    <View style={[styles.Discription, { marginTop: hp('2%') }]}>
                        <Text style={styles.Font1}>Description</Text>
                        <Text style={styles.Font2}>
                            Lorem Ipsum is simply dummy text of the{"\n"}printing and
                            typesetting industry. Lorem{"\n"}Ipsum has been the industry's
                            standard{"\n"}dummy text ever since the 1500s
                        </Text>
                    </View>

                    <View style={styles.Discription}>
                        <Text style={styles.Font1}>Dj’s Upcoming Events:</Text>

                    </View>
                    <View style={styles.eventWrapper}>
                        <EventCard />
                    </View>
                </View>
            </ScrollView>
            <BottomMenu navigation={navigation} />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff",
        alignItems: "center",
    },
    eventWrapper: {
        width: wp("100%"),
        alignItems: "center",
        paddingHorizontal: wp('5%'),
        marginTop:hp('2%')
    },
    wrapper: {
        width: wp("100%"),
        alignItems: "center",
        marginBottom: "17%",
    },
    Img: {
        width: wp("90%"),
        height: hp("20%"),
        backgroundColor: "red",
        marginTop:10,
        borderRadius:10,
        overflow:'hidden'
    },
    Discription: {
        width: wp("90%"),
        paddingVertical: 10,

    },
    Font1: {
        fontSize: rf(14),
        fontFamily: 'MB',
        letterSpacing: 0.8,
        color: "#FF5623",
    },
    Font2: {
        fontSize: rf(12),
        fontFamily: 'MM',
        letterSpacing: 1,
        color: "#525151",
        marginTop: 5
    },
    title: {
        width: wp("100%"),
        height: hp("5%"),
        justifyContent: "center",
        backgroundColor: "#E5E5E5",
        paddingHorizontal: "5%",
    },
    titleFont: {
        fontSize: rf(13),
        fontFamily: 'MB',
        letterSpacing: 0.8,
    },
    PhotosView: {
        height: hp("24%"),
        flexDirection: "row",
        marginTop: 5
    },
    DotsWrapper: {
        width: "100%",
        height: "25%",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
    },
    Dot1: {
        width: hp("1%"),
        height: hp("1%"),
        borderRadius: 100,
        borderWidth: 0,
        backgroundColor: "#FD581F",
        margin: 2,
    },
    Dot2: {
        width: hp("1%"),
        height: hp("1%"),
        borderRadius: 100,
        borderWidth: 2,
        borderColor: "#FD581F",
        backgroundColor: "#fff",
        margin: 2,
    },
    videoContainer: {
        width: "100%",
        height: hp("18%"),
        marginVertical: 10
    },
    video: {
        width: "100%",
        height: hp("18%"),
    },
    map: {
        width: wp("100%"),
        height: hp("27%"),
    },
});
