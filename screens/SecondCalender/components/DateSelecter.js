import React from 'react';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { RFValue as rf } from "react-native-responsive-fontsize";
import { StyleSheet, Text, View, TextInput } from 'react-native';
import { AntDesign } from '@expo/vector-icons';
export default function DateSelecter({ navigation }) {
    return (
        <View style={styles.container}>
            <View style={styles.CalenderIConBody}>
                <AntDesign name="calendar" size={rf(22)} color="#FF5623" />
            </View>
            <Text style={styles.DateSelecText}>Select Date</Text>
            <TextInput placeholder='e.g 19/19/2021' placeholderTextColor='#525151'
                style={styles.InputDateBody} />

        </View>

    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        borderTopWidth: 2,
        borderBottomWidth: 2,
        borderColor: '#B6ADAD',
        alignItems: 'center',
        justifyContent: 'center'
    },
    CalenderIConBody: {
        height: hp('6%'),
        width: hp('6%'),
        justifyContent: 'center',
        alignItems: 'center',

    },
    DateSelecText: {
        fontSize: rf(14),
        fontWeight: '400',
        marginBottom:10

    },
    InputDateBody: {
        borderBottomWidth: 1,
        borderColor: '#525151',
        fontSize: rf(12),

    }



});
