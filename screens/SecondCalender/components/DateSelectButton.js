import React from "react";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
export default function DateSelectButton(props) {
  return (
    <TouchableOpacity style={styles.SelectDateBtn}>
      <Text style={styles.BtnText}>{props.title}</Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  SelectDateBtn: {
    height: hp("6%"),
    width: wp("43%"),
    borderWidth: 2,
    borderColor: "#FF5623",
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center",
  },
  BtnText: {
    fontWeight: "700",
    fontSize: rf(12),
  },
});
