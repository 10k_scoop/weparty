import React from "react";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Image,
  ScrollView,
} from "react-native";
import Header from "../../components/Header";
import BottomMenu from "../../components/BottomMenu";
import DateSelecter from "./components/DateSelecter";
import DateSelectButton from "./components/DateSelectButton";
export default function SecondCalender({ navigation }) {
  return (
    <View style={styles.container}>
      <Header
        BackTxt="Back"
        Title="Calender"
        navigation={() => navigation.goBack()}
      />
      <ScrollView>
        <View style={styles.DateSelecterWrapper}>
          <DateSelecter />
        </View>
        <View style={styles.CalenderWrapper}>
          <Image
            source={require("../../assets/Calender.png")}
            style={{ height: "100%", width: "100%" }}
            resizeMode="stretch"
          />
        </View>
        <View style={styles.SelectDateBtnWrapper}>
          <DateSelectButton title="Select specific date" />
          <DateSelectButton title="Select specific date" />
        </View>
        <TouchableOpacity style={styles.SearchEventBtn}>
          <Text style={styles.SeacrhEventText}>Search event</Text>
        </TouchableOpacity>
      </ScrollView>

      <BottomMenu navigation={navigation} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  DateSelecterWrapper: {
    height: hp("18%"),
    paddingHorizontal: wp("4%"),
  },
  CalenderWrapper: {
    height: hp("38%"),
    paddingHorizontal: wp("4%"),
  },
  SelectDateBtnWrapper: {
    height: hp("14%"),
    width: wp("100%"),
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
  },
  SearchEventBtn: {
    height: hp("5%"),
    width: wp("30%"),
    borderWidth: 2,
    borderColor: "#FF5623",
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
  },
  SeacrhEventText: {
    fontWeight: "700",
    fontSize: rf(12),
  },
});
