import React from 'react';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { RFValue as rf } from "react-native-responsive-fontsize";
import { StyleSheet, Text, TouchableOpacity, View, Image } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';
export default function MusicCategory(props) {
    return (
        <View style={styles.container}>
            <Text style={styles.MusicText}>{props.Title}</Text>
        </View>

    );
}

const styles = StyleSheet.create({
    container: {
        height:hp('6%'),
        backgroundColor: '#fff',
        borderBottomWidth: 1,
        borderColor: '#E5E5E5',
        flexDirection: 'row',
        alignItems:'flex-end',
        paddingHorizontal: '4%',
        justifyContent:'space-between'
    },
    MusicText: {
        fontSize: rf(12),
        fontWeight: '400',
        marginBottom:5
    }





});
