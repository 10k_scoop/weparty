import React from 'react';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { RFValue as rf } from "react-native-responsive-fontsize";
import { StyleSheet, View, ScrollView } from 'react-native';
import BottomMenu from '../../components/BottomMenu';
import Header from './components/Header';
import MusicCategory from './components/MusicCategory';
export default function FilterClub({ navigation }) {
    return (
        <View style={styles.container}>
            <Header />
            <View style={styles.PartyCatpgeryWrapper}>
                <MusicCategory Title='WeParty' />
                <MusicCategory Title='Lorem Ipsum' />
                <MusicCategory Title='Lorem Ipsum' />

            </View>

            <BottomMenu />
        </View>

    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    PartyCatpgeryWrapper: {
        justifyContent: 'center',
        paddingHorizontal: wp('4%'),
        marginTop:hp('5%')

    }




});
