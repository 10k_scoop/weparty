import React from "react";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { StyleSheet, Text, View } from "react-native";
import Header from "../../components/Header";
import ProfilePicEdit from "./components/ProfilePicEdit";
import ProfileDetailEdit from "./components/ProfileDetailEdit";
import BottomMenu from "../../components/BottomMenu";
export default function EditProfile({ navigation }) {
  return (
    <View style={styles.container}>
      <Header
        BackTxt="Back"
        Title="Profile"
        navigation={() => navigation.goBack()}
      />
      <View style={styles.EditProfileWrapper}>
        <ProfilePicEdit Image={require("../../assets/ProfilePhoto.png")} />
      </View>
      <View style={styles.ProfileDetailEditWrapper}>
        <ProfileDetailEdit Title="Name:" TitleGiven="Atif" />
        <ProfileDetailEdit Title="Email:" TitleGiven="Atif@gmail.com" />
        <ProfileDetailEdit Title="City:" TitleGiven="Islamabad"  />
        <ProfileDetailEdit Title="Password:" TitleGiven="*******" />
        <ProfileDetailEdit Title="Relationship Status:" TitleGiven="Single" />
      </View>
      <BottomMenu navigation={navigation} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  EditProfileWrapper: {
    height: hp("32%"),
    paddingHorizontal: wp("4%"),
  },
  ProfileDetailEditWrapper: {
    height: hp("25%"),
    paddingHorizontal: wp("6%"),
    justifyContent: "center",
  },
});
