import React from 'react';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { RFValue as rf } from "react-native-responsive-fontsize";
import { StyleSheet, Text, TouchableOpacity, View, Image } from 'react-native';
export default function ProfilePicEdit(props) {
    return (
        <View style={styles.ProfileViewBars}>
            <View style={styles.ProfilePicBody}>
                <Image source={props.Image}
                    style={{ height: '100%', width: '100%' }}
                    resizeMode='contain' />
            </View>
            <TouchableOpacity onPress={props.click} style={styles.ProfileEditBtn}>
                <Text style={styles.BtnText}>Change Profile Image</Text>
            </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
    ProfileViewBars: {
        height: '100%',
        width: '100%',
        borderBottomWidth: 2,
        borderTopWidth: 2,
        borderBottomColor: '#FF5623',
        borderTopColor: '#FF5623',
        alignItems: 'center',
        justifyContent: 'center'

    },
    ProfilePicBody: {
        height: hp('10%'),
        width: hp('10%'),
        borderRadius: 100,
        overflow: 'hidden',
        marginBottom: '10%'

    },
    ProfileEditBtn: {
        height: '14%',
        width: '42%',
        borderRadius: 10,
        borderWidth: 2,
        borderColor: '#FF5623',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: '5%'
    },
    BtnText: {
        fontWeight: '600',
        fontSize: rf(12),
        color: '#222'
    }









});
