import React from 'react';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { RFValue as rf } from "react-native-responsive-fontsize";
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
export default function ProfileDetailEdit(props) {
    return (
        <View style={styles.ProfileDetailBody}>
            <View style={styles.NameBody}>
                <Text style={styles.NameText}>{props.Title}</Text>
            </View>
            <View style={styles.GivenNameBody}>
                <Text style={styles.Name}>{props.TitleGiven}</Text>
            </View>
            <TouchableOpacity>
                <Text style={styles.EditBtnTxt}>Edit</Text>
            </TouchableOpacity>
        </View>

    );
}

const styles = StyleSheet.create({
    ProfileDetailBody: {
        flex: 0.28,
        flexDirection: 'row',
        alignItems: 'center',
    },
    NameBody: {
        flex: 0.3,
        justifyContent:'center'
    },
    GivenNameBody: {
        flex: 0.7,
        justifyContent:'center'
    },
    NameText: {
        fontSize: rf(12),
        fontWeight: '700',
    },
    Name: {
        fontSize: rf(12),
        fontWeight: '400'
    },
    EditBtnTxt: {
        fontSize: rf(12),
        fontWeight: '700',
        color: '#FF5623'

    }


});
