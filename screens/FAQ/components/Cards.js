import React from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";

export default function Cards(props) {
  return (
    <TouchableOpacity style={styles.container}>
      <Text style={styles.Font}>{props.txt}</Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("95%"),
    height: hp("6%"),
    borderBottomWidth: 2,
    borderColor: "#e5e5e5",
    justifyContent: "center",
  },
  Font: {
    fontSize: rf(14),
    fontWeight: "400",
    color: "#525151",
  },
});
