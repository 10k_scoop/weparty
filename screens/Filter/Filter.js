import React,{useState} from "react";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Image,
  ScrollView,
} from "react-native";
import BottomMenu from "../../components/BottomMenu";
import Header from "./components/Header";
import { AntDesign } from "@expo/vector-icons";
export default function Filter({ navigation }) {
  const [checkBox,setCheckBox]=useState(false)
  return (
    <View style={styles.container}>
      <Header />
      <View style={styles.PickerWrapper}>
        <TouchableOpacity style={styles.PickerBody} onPress={() => navigation.navigate("FilterMusic")}>
          <Text style={styles.PickerText}>Genre</Text>
          <View onPress={() => navigation.navigate("FilterMusic")}>
            <AntDesign name="right" size={rf(14)} color="black" />
          </View>
        </TouchableOpacity>
        <TouchableOpacity style={styles.PickerBody}>
          <Text style={styles.PickerText}>Clubs</Text>
          <View>
            <AntDesign name="right" size={rf(14)} color="black" />
          </View>
        </TouchableOpacity>
        <View style={styles.PickerBody}>
          <Text style={styles.PickerText}>LGBT+</Text>
          <TouchableOpacity onPress={()=>setCheckBox(!checkBox)} style={styles.lgbtCheckBox}>
            {checkBox && <AntDesign name="check" size={rf(14)} color="black" />}
          </TouchableOpacity>
        </View>
        <TouchableOpacity onPress={() => navigation.navigate("Bars")} style={styles.PickerBody}>
          <Text style={styles.PickerTexxt}>Bars</Text>
          <View>
            <AntDesign name="right" size={rf(14)} color="black" />
          </View>
        </TouchableOpacity>
      </View>

      <BottomMenu navigation={navigation} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  lgbtCheckBox:{
    width:20,
    height:20,
    borderWidth:1,
    borderColor:'#222',
    alignItems:'center',
    justifyContent:'center',
    borderRadius:2
  },
  PickerWrapper: {
    height: hp("23%"),
    paddingHorizontal: wp("4%"),
    justifyContent: "space-evenly",
    alignItems: "center",
    marginTop: hp("10%"),
  },
  PickerBody: {
    height: hp("5%"),
    width: "100%",
    borderRadius: 10,
    borderWidth: 2,
    borderColor: "#FD581F",
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: "5%",
    justifyContent: "space-between",
  },
  PickerText: {
    fontSize: rf(12),
    fontWeight: "400",
  },
});
