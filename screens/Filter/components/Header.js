import React from 'react';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { RFValue as rf } from "react-native-responsive-fontsize";
import { Ionicons } from '@expo/vector-icons';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
export default function Header(props) {
    return (
        <View style={styles.container}>
            <View style={styles.IconBody}>
                <Text style={styles.IconText}>i</Text>
            </View>
            <Text style={styles.FilterText}>Filter by</Text>
            <TouchableOpacity>
                <Text style={styles.DoneText}>Done</Text>
            </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        height: hp('14%'),
        paddingHorizontal: wp('4%'),
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    IconBody: {
        height: 20,
        width: 20,
        borderRadius: 100,
        backgroundColor: '#FD581F',
        justifyContent: 'center',
        alignItems: 'center'
    },
    IconText: {
        fontWeight: '700',
        fontSize: rf(13),
        color: '#fff'
    },
    FilterText: {
        fontWeight: '700',
        fontSize: rf(14),
        color: '#222'
    },
    DoneText: {
        fontWeight: '400',
        fontSize: rf(14),
        color: '#FD581F'
    }









});
