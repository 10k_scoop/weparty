import React from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { AntDesign, Feather } from "@expo/vector-icons";

export default function Header(props) {
  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.Icon} onPress={props.onBackClick}>
        <View onPress={props.onBackClick}>
          <AntDesign name="left" size={rf(18)} color="#FD581F" />
        </View>
        <Text style={styles.Font1}>Back</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("100%"),
    height: hp("10%"),
  },
  Icon: {
    width: "100%",
    height: "100%",
    flexDirection: "row",
    alignItems: "flex-end",
    paddingVertical: "2%",
    paddingHorizontal: "2%",
  },
  Font1: {
    fontSize: rf(14),
    fontWeight: "bold",
    color: "#FD581F",
    marginLeft: 4,
  },
});
