import React from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { Feather } from "@expo/vector-icons";
import BottomMenu from "../../components/BottomMenu";
import Header from "./components/Header";
import PartyCards from "./components/PartyCards";
import MapView from "react-native-maps";

export default function ThirdCalender({ navigation }) {
  return (
    <View style={styles.container}>
      <Header onBackClick={() => navigation.goBack()} />
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.Wrapper}>
          <View style={styles.DateRow}>
            <Text>19/10/2021</Text>
            <TouchableOpacity onPress={() => navigation.navigate("Calender")}>
              <Feather name="calendar" size={rf(20)} color="#FD581F" />
            </TouchableOpacity>
          </View>
          <View style={styles.MapView}>
            <MapView
              style={styles.map}
              region={{
                latitude: 52.377956,
                longitude: 4.88969,
                latitudeDelta: 0.015,
                longitudeDelta: 0.0121,
              }}
            />
          </View>
          <PartyCards date="November 14, 2021" />
          <PartyCards />
        </View>
      </ScrollView>
      <BottomMenu navigation={navigation} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  Wrapper: {
    width: wp("100%"),
    alignItems: "center",
    marginBottom: "20%",
  },
  DateRow: {
    width: wp("90%"),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginBottom:hp('2%')
  },
  MapView: {
    width: wp("100%"),
    height: hp("32%"),
    marginBottom:10
  },
  map: {
    flex: 1,
  },
});
