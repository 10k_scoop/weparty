import React from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  TextInput,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { AntDesign } from "@expo/vector-icons";
import BottomMenu from "../../components/BottomMenu";
import Header from "./components/Header";

export default function Security({ navigation }) {
  return (
    <View style={styles.container}>
      <Header onBack={() => navigation.goBack()} />
      {/* Form */}
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.FormRow}>
          <View style={styles.FormInner}>
            <Text style={styles.FormTxt}>Form</Text>
            <Text style={styles.RequiredTxt}>Required</Text>
          </View>
          <View style={styles.FormField}>
            <View style={styles.FormLayer}></View>
            <TextInput style={styles.TextField} />
          </View>
        </View>
        {/* Form */}

        {/* Reason */}
        <View style={styles.ReasonRow}>
          <View style={styles.FormInner}>
            <Text style={styles.FormTxt}>Reason</Text>
            <Text style={styles.RequiredTxt}>Required</Text>
          </View>
          <View style={styles.ReasonField}>
            <View style={styles.FormLayer}></View>
            <TouchableOpacity
              style={styles.Icon}
              onPress={() => navigation.navigate("Reason")}
            >
              <AntDesign name="down" size={rf(18)} color="black" />
            </TouchableOpacity>
          </View>
        </View>
        {/* Reason */}

        {/* Messages */}
        <View style={styles.MessagesRow}>
          <Text style={styles.MsgTxt}>Messages</Text>
          <View style={styles.MsgBox}>
            <View style={styles.FormLayer}></View>
            <TextInput style={styles.MsgField} multiline={true} />
          </View>
        </View>
      </ScrollView>
      {/* Messages */}

      <BottomMenu navigation={navigation} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  FormRow: {
    width: wp("90%"),
    height: hp("10%"),
    justifyContent: "space-between",
    marginTop: "10%",
  },
  FormInner: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
  },
  FormField: {
    width: "100%",
    height: "70%",
    borderRadius: 10,
    overflow: "hidden",
  },
  FormLayer: {
    width: "100%",
    height: "100%",
    position: "absolute",
    backgroundColor: "#E5E5E5",
    opacity: 0.4,
  },
  TextField: {
    width: "100%",
    height: "100%",
    paddingHorizontal: 5,
    fontSize: rf(16),
  },
  FormTxt: {
    fontSize: rf(14),
    fontWeight: "400",
    color: "#525151",
  },
  RequiredTxt: {
    fontSize: rf(14),
    fontWeight: "400",
    color: "#525151",
  },
  MessagesRow: {
    width: wp("90%"),
    height: hp("35%"),
    marginTop: "5%",
    justifyContent: "space-between",
  },
  MsgTxt: {
    fontSize: rf(14),
    fontWeight: "400",
    color: "#525151",
  },
  MsgBox: {
    width: "100%",
    height: "90%",
    borderRadius: 10,
    overflow: "hidden",
  },
  MsgField: {
    width: "100%",
    padding: 10,
    fontSize: rf(16),
  },
  ReasonRow: {
    width: wp("90%"),
    height: hp("10%"),
    justifyContent: "space-between",
    marginTop: "5%",
  },
  ReasonField: {
    width: "100%",
    height: "70%",
    borderRadius: 10,
    overflow: "hidden",
    alignItems: "flex-end",
    justifyContent: "center",
  },
  Icon: {
    marginRight: 5,
  },
});
