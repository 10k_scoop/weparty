import React from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { AntDesign } from "@expo/vector-icons";

export default function Header(props) {
  return (
    <View style={styles.container}>
      <View style={styles.BackRow}>
        <TouchableOpacity onPress={props.onBack}>
          <AntDesign name="left" size={rf(16)} color="#FF5623" />
        </TouchableOpacity>
        <Text style={styles.Font2}>Back</Text>
      </View>

      <Text style={styles.Font1}>Report this app</Text>
      <TouchableOpacity>
        <Text style={styles.Font3}>Send</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("100%"),
    height: hp("10%"),
    flexDirection: "row",
    alignItems: "flex-end",
    justifyContent: "space-between",
    paddingHorizontal: "3%",
  },
  title: {
    width: "85%",
    height: "100%",
    alignItems: "center",
    justifyContent: "flex-end",
  },
  Font1: {
    fontSize: rf(16),
    fontWeight: "bold",
    marginRight: "5%",
    color: "#525151",
  },
  BackRow: {
    flexDirection: "row",
    width: "15%",
    justifyContent: "space-between",
  },
  Font2: {
    fontSize: rf(14),
    fontWeight: "bold",
    color: "#FF5623",
  },
  Font3: {
    fontSize: rf(14),
    fontWeight: "bold",
    color: "#FF5623",
  },
});
